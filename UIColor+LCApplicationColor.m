//
//  UIColor+LCApplicationColor.m
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "UIColor+LCApplicationColor.h"

@implementation UIColor (LCApplicationColor)

+ (UIColor *)createColorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue A:(CGFloat)alpha {
    
    return [UIColor colorWithRed:red/255.f green:green/255.f blue:blue/255.f alpha:alpha];
}

+ (UIColor *)applicationBarColor {
    
    return [self createColorWithR:161.f G:193.f B:218.f A:1.f];
}

+ (UIColor *)applicationMainTextColor {
    
    return [self createColorWithR:161.f G:196.f B:216.f A:1.f];
}

+ (UIColor *)applicationMainBackgroundColor {
    
    return [self createColorWithR:68.f G:74.f B:92.f A:1.f];
}

+ (UIColor *)applicationLabelColor {
    
    return [self createColorWithR:184.f G:222.f B:250.f A:1.f];
}

+ (UIColor *)applicationNameCompanyThreeCell {
    
    return [self createColorWithR:87.f G:92.f B:108.f A:1.f];
}

#pragma mark - SegmentsColor

+ (UIColor *)applicationSegmentsIsHighlights {
    
    return [self createColorWithR:87.f G:120.f B:164.f A:1.f];
}

+ (UIColor *)applicationSegmentsNopeHighlights {
    
    return [self createColorWithR:184.f G:222.f B:250.f A:1.f];
}

#pragma mark - SeparatorNameCompanyColor

+ (UIColor *)applicationSeparatorNameCompanyColor {
    
    return [self createColorWithR:38.f G:42.f B:53.f A:1.f];
}

#pragma mark - ButtonCallMeColors

+ (UIColor *)applicationCallMeLabelColor {
   
    return [self createColorWithR:15.f G:66.f B:111.f A:1.f];
}

+ (UIColor *)applicationCallMeButtonColor {
    
    return [self createColorWithR:99.f G:182.f B:255.f A:1.f];
}


#pragma mark - CreditsController

+ (UIColor *)applicationUpBackgroundSegmentColor {
    
    return [self createColorWithR:212.f G:216.f B:233.f A:1.f];
}

#pragma mark - ColorTabBarItems

+ (UIColor *)applicationTabBarItemsColor {
    
    return [self createColorWithR:130.f G:183.f B:255.f A:1.f];
}

#pragma mark - WindowPushCalc

+ (UIColor *)applicationMainViewColor {
    
    return [self createColorWithR:255.f G:255.f B:255.f A:0.33f];
}

+ (UIColor *)applicationTextCalcColor {
    
    return [self createColorWithR:62.f G:71.f B:99.f A:1.f];
}

+ (UIColor *)applicationCalcLabelColor {
    
    return [self createColorWithR:190.f G:222.f B:250.f A:1.f];
}

+ (UIColor *)applicationCalcColorCell {
    
    return [self createColorWithR:59.f G:64.f B:80.f A:1.f];
}

@end
