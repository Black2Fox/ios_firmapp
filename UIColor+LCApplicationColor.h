//
//  UIColor+LCApplicationColor.h
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (LCApplicationColor)

#pragma mark - CompanyColor

+ (UIColor *)applicationBarColor;
+ (UIColor *)applicationMainTextColor;
+ (UIColor *)applicationMainBackgroundColor;
+ (UIColor *)applicationLabelColor;
+ (UIColor *)applicationNameCompanyThreeCell;

#pragma mark - SegmentsColor

+ (UIColor *)applicationSegmentsIsHighlights;
+ (UIColor *)applicationSegmentsNopeHighlights;

#pragma mark - SeparatorNameCompanyColor

+ (UIColor *)applicationSeparatorNameCompanyColor;

#pragma mark - ButtonCallMeColors

+ (UIColor *)applicationCallMeLabelColor;
+ (UIColor *)applicationCallMeButtonColor;


+ (UIColor *)applicationUpBackgroundSegmentColor;
+ (UIColor *)applicationTabBarItemsColor;

#pragma mark - Calc

+ (UIColor *)applicationMainViewColor;
+ (UIColor *)applicationTextCalcColor;
+ (UIColor *)applicationCalcLabelColor;
+ (UIColor *)applicationCalcColorCell;
@end
