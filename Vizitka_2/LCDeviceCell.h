//
//  LCDeviceCell.h
//  Vizitka_2
//
//  Created by Vio on 22.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCDeviceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *iOSButton;
@property (weak, nonatomic) IBOutlet UIButton *androidButton;
@property (weak, nonatomic) IBOutlet UILabel *iosLabel;

@property (weak, nonatomic) IBOutlet UILabel *androidLabel;
@end
