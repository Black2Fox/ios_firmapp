//
//  LCHelpPush.h
//  Vizitka_2
//
//  Created by Vio on 22.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LCMainPriceCell.h"
#import "LCCallbackCell.h"
#import "LCNumberAndEmailCell.h"

static NSString* const kIdentifireMainPriceCell = @"mainPriceCell";
static NSString* const kIdentifireCallbackCell = @"callBackCell";
static NSString* const kIdentifireNumberEmailCell = @"numberAndEmailCell";

@interface LCHelpPush : NSObject

+ (void)settingPriceCell:(LCMainPriceCell *)cell;
+ (void)settingCallbackCell:(LCCallbackCell *)cell;
+ (void)settingNumberAndEmailCell:(LCNumberAndEmailCell *)cell;

@end
