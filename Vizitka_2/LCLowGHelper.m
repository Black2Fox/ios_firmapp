//
//  LCLowGHelper.m
//  Vizitka_2
//
//  Created by Vio on 17.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCLowGHelper.h"

#import "UIButton+LCCustomProperty.h"

@implementation LCLowGHelper

+ (UILabel *)settingFirstLabel:(CGRect)rect andText:(NSString *)text andPercent:(CGFloat)percent{
    
    UILabel* firstLabel = [[UILabel alloc] initWithFrame:rect];
    
    firstLabel.text = text;
    firstLabel.font = [UIFont systemFontOfSize:10.7f * percent];
    firstLabel.textColor = [UIColor whiteColor];
    firstLabel.textAlignment = NSTextAlignmentCenter;
    
    return firstLabel;
}

+ (UILabel *)settingSecondLabel:(CGRect)rect andText:(NSString *)text andPercent:(CGFloat)percent{
    
    UILabel* secondLabel = [[UILabel alloc] initWithFrame:rect];
    
    secondLabel.text = text;
    secondLabel.font = [UIFont systemFontOfSize:10.7f * percent];
    secondLabel.textColor = [UIColor whiteColor];
    secondLabel.numberOfLines = 2;
    
    return secondLabel;
    
}
- (UIButton *)settingButton:(CGRect)rect andText:(NSString *)text andPercent:(CGFloat)percent andURL:(NSURL *)url{
    
    UIButton* button = [[UIButton alloc] initWithFrame:rect];
    
    [button setTitle:text forState:UIControlStateNormal];
    button.titleLabel.textColor = [UIColor blackColor];
    button.titleLabel.font = [UIFont systemFontOfSize:13 * percent];
    button.layer.cornerRadius = 4.f;
    button.layer.masksToBounds = YES;
    button.layer.borderWidth = 1.f;
    button.layer.borderColor = ([UIColor whiteColor].CGColor);
    button.backgroundColor = [UIColor clearColor];
    [button setCustomUrlValue:url];
    
    [button addTarget:self action:@selector(tabButtonWithParameter:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
    
}

- (void)tabButtonWithParameter:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(tabButtonWithParameter:)]) {
        
        [self.delegate tabButtonWithParameter:sender];
    }
}



@end
