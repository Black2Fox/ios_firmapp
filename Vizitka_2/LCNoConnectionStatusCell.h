//
//  LCNoConnectionStatusCell.h
//  Vizitka_2
//
//  Created by Vio on 02.12.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LCNoConnectionDelegate;

@interface LCNoConnectionStatusCell : UITableViewCell

@property (nonatomic) id<LCNoConnectionDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *labelStatus;
@property (weak, nonatomic) IBOutlet UIButton *buttonStatus;

@end


@protocol LCNoConnectionDelegate <NSObject>

@optional

- (IBAction)actionReloadController:(id)sender;

@end
