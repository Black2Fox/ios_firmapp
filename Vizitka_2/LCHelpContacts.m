//
//  LCHelpContacts.m
//  Vizitka_2
//
//  Created by Vio on 08.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCHelpContacts.h"

@implementation LCHelpContacts

+ (void)settingSegmentCell:(LCSegmentCell *)cell {
    
    cell.timeLabel.textColor = [UIColor applicationLabelColor];
    
    cell.contentView.backgroundColor = [UIColor applicationMainBackgroundColor];
    
    [cell.timeSegment setTintColor:[UIColor applicationSegmentsNopeHighlights]];
    
    
    UIFont *font = [UIFont systemFontOfSize:10.5f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    [cell.timeSegment setTitleTextAttributes:attributes
                                    forState:UIControlStateNormal];
    
}

+ (void)settingNameCompanyCell:(LCNameCompanyCell *)cell {
    
    cell.nameCompanyLabel.textColor = [UIColor applicationMainTextColor];
    cell.mainViewCompany.backgroundColor = [UIColor applicationNameCompanyThreeCell];
    
    cell.mainViewCompany.layer.cornerRadius = 5.f;
    cell.mainViewCompany.layer.masksToBounds = YES;
    
    cell.mainContentView.backgroundColor = [UIColor applicationMainBackgroundColor];
        
    cell.phoneLabel.textColor = [UIColor applicationLabelColor];
    cell.skypeLabel.textColor = [UIColor applicationLabelColor];
    cell.emailLabel.textColor = [UIColor applicationLabelColor];
    
    cell.separatorOne.backgroundColor = cell.separatorTwo.backgroundColor = cell.separatorThree.backgroundColor = [UIColor applicationSeparatorNameCompanyColor];
    
    cell.otherLabel.textColor = [UIColor applicationMainTextColor];
    
    //cell.constructPrefixView.backgroundColor = [UIColor applicationNameCompanyThreeCell];
    
}
+ (void)settingPhoneCell:(LCPhoneCell *)cell {
    
    cell.contentMainView.backgroundColor = [UIColor applicationMainBackgroundColor];
    cell.labelPhone.textColor = [UIColor applicationLabelColor];
    cell.nameLabel.textColor = [UIColor applicationLabelColor];
    
}
+ (void)settingAttachmentCell:(LCAttachmentCell *)cell {
    
    cell.attachmentLabel.textColor = [UIColor applicationLabelColor];
    
    cell.contentView.backgroundColor = [UIColor applicationMainBackgroundColor];
    
    cell.contentView.backgroundColor = [UIColor applicationMainBackgroundColor];
    
    [cell.buttonCollect setTintColor:[UIColor applicationCallMeLabelColor]];
    cell.buttonCollect.backgroundColor = [UIColor applicationCallMeButtonColor];
    
    cell.buttonCollect.layer.masksToBounds = YES;
    cell.buttonCollect.layer.cornerRadius = 5.f;
    
    [cell.attachTextView.layer setCornerRadius:5.0f];
    [cell.attachTextView.layer setMasksToBounds:YES];
}

+ (void)settingSocialImageCell:(LCSocialImageCell *)cell {
    
    cell.contentView.backgroundColor = [UIColor applicationMainBackgroundColor];
    
}

@end
