//
//  LCPublicationAndAssistanceCell.h
//  Vizitka_2
//
//  Created by Vio on 25.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCPublicationAndAssistanceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *buttonAppstore;
@property (weak, nonatomic) IBOutlet UIButton *buttonDeeplink;
@property (weak, nonatomic) IBOutlet UIButton *buttonSearch;
@property (weak, nonatomic) IBOutlet UILabel *appstoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *deeplinkLabel;
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;

@end
