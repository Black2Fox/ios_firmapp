//
//  LCAuthCell.m
//  Vizitka_2
//
//  Created by Vio on 23.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCAuthCell.h"

@implementation LCAuthCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    UIImage* normalOvel = [UIImage imageNamed:@"oval_back"];
    UIImage* selectedOval = [UIImage imageNamed:@"oval_high"];
    
    [self.buttonSocial setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonSocial setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonCabinet setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonCabinet setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonSocial addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buttonCabinet addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];

}

- (void)buttonTouch:(UIButton *)aButton withEvent:(UIEvent *)event {
    aButton.selected = !aButton.selected;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
