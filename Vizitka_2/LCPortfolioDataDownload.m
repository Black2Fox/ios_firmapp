//
//  LCPortfolioDataDownload.m
//  Vizitka_2
//
//  Created by Vio on 30.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCPortfolioDataDownload.h"
#import "Backendless.h"

@implementation LCPortfolioDataDownload

+ (NSArray *)downloadContentFromServer {
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[LCPortfolioDataDownload class]];
    BackendlessCollection *collection = [dataStore find:nil];
    
    if (collection.data == nil) {
        NSArray* array = [NSArray new];
        return array;
    } else {
        return collection.data;
    }
}



+ (LCPortfolioDataDownload *)hello:(NSInteger)one hello:(NSInteger)two hello:(NSInteger)three{
    LCPortfolioDataDownload *image = [LCPortfolioDataDownload new];
    image.red = one;
    
    
    return image;
}

+ (LCPortfolioDataDownload *)hello:(NSString *)one {
    LCPortfolioDataDownload *image = [LCPortfolioDataDownload new];
    image.urlDownloadApp = one;
    
    return image;
}

@end
