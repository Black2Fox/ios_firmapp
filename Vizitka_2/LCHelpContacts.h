//
//  LCHelpContacts.h
//  Vizitka_2
//
//  Created by Vio on 08.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LCSegmentCell.h"
#import "LCNameCompanyCell.h"
#import "LCPhoneCell.h"
#import "LCAttachmentCell.h"
#import "LCSocialImageCell.h"

static NSString* const kDefaultTitleLeft = @"О нас";
static NSString* const kIdentifireSegmentCell = @"segmentCell";
static NSString* const kIdentifireNameCompanyCell = @"nameCompanyCell";
static NSString* const kIdentifirePhoneCell = @"phoneCell";
static NSString* const kIdentifireAttachmentCell = @"attachmentCell";
static NSString* const kIdentifireSocialCell = @"socialCell";

@interface LCHelpContacts : NSObject

+ (void)settingSegmentCell:(LCSegmentCell *)cell;
+ (void)settingNameCompanyCell:(LCNameCompanyCell *)cell;
+ (void)settingPhoneCell:(LCPhoneCell *)cell;
+ (void)settingAttachmentCell:(LCAttachmentCell *)cell;
+ (void)settingSocialImageCell:(LCSocialImageCell *)cell;

@end
