//
//  LCWebView.h
//  Vizitka_2
//
//  Created by Vio on 17.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCWebView : UIViewController

@property (strong, nonatomic) NSURL* urlData;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;

@end
