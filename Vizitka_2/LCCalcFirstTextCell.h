//
//  LCCalcFirstTextCell.h
//  Vizitka_2
//
//  Created by Vio on 22.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCCalcFirstTextCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *firstTextLabel;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) NSArray* tempArray;
@end
