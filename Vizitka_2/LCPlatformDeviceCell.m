//
//  LCPlatformDeviceCell.m
//  Vizitka_2
//
//  Created by Vio on 23.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCPlatformDeviceCell.h"

@implementation LCPlatformDeviceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    UIImage* normalOvel = [UIImage imageNamed:@"oval_back"];
    UIImage* selectedOval = [UIImage imageNamed:@"oval_high"];
    
    [self.buttonPhone setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonPhone setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonTablet setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonTablet setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonWatch setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonWatch setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonPhone addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buttonTablet addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buttonWatch addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)buttonTouch:(UIButton *)aButton withEvent:(UIEvent *)event {
    aButton.selected = !aButton.selected;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
