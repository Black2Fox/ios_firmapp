//
//  LCConnectionStatus.h
//  Vizitka_2
//
//  Created by Vio on 16.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LCConnectionStatus : NSObject

+ (BOOL)statusConnected;

+ (UIAlertController *)noInternetConnection;

@end
