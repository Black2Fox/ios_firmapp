//
//  UIButton+LCCustomProperty.h
//  Vizitka_2
//
//  Created by Vio on 17.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (LCCustomProperty)

@property (nonatomic, retain) NSURL* customUrlValue;
@property (nonatomic, retain) NSString* priceVolume;

@end
