//
//  LCAlertService.h
//  Vizitka_2
//
//  Created by Vio on 25.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>


//Alert Const
static NSString* const kError = @"Ошибка!";
static NSString* const kWarning = @"Внимание!";
static NSString* const kNotSelectedOptions = @"Не выбраны варианты!";
static NSString* const kNotSelectedPlatform = @"Не выбрана платформа! \n Выберите платформу для приложения!";
static NSString* const kNotSelectedDevice = @"Не выбрано устройство! \n Выберите устройство для приложения!";
static NSString* const kLostConnection = @"Нет соединения с интернетом!";
static NSString* const kFalseFilledEmail = @"Неверно заполнен e-mail адрес";
static NSString* const kFalseFilledPhone = @"Неверно заполнен контактный телефон";
static NSString* const kFalseFilledName = @"Неверно заполнено имя";
static NSString* const kSuccessSendTicket = @"Ваша заявка успешно отправлена!";
static NSString* const kWaitCallCompany = @"Ожидайте звонка специалиста.\nСпасибо за обращение в нашу компанию!";
static NSString* const kMissedMailAgent = @"Пустой почтовый агент!";
static NSString* const kMissedSkypeAgent = @"Не найден скайп";
static NSString* const kDownloadMessage = @"Скачаем?";
static NSString* const kSizeFile = @"Размер загружаемого файла не должен превышать 30 Мб";
static NSString* const kAppStore = @"Перейти в AppStore?";


#pragma mark - Interface

@interface LCAlertService : NSObject

- (void)showAlertButtonOkWithTitle:(NSString *)title andMessage:(NSString *)message inVC:(UIViewController *)view;

+ (instancetype)instance;

- (void)showAlertSkypeInVC:(UIViewController *)view;

- (void)showAlertAppStoreInVC:(UIViewController *)view andLink:(NSURL *)link;

@end
