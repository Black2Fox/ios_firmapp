//
//  LCCalculatorController.h
//  Vizitka_2
//
//  Created by Vio on 14.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LCCalcConst.h"

typedef NS_ENUM(NSInteger, LCSectionTransfer){
    
    LCSectionFirstText                  = 0,
    LCSectionPlatform                   = 1,
    LCSectionDevice                     = 2,
    LCSectionAuthorization              = 3,
    LCSectionAdditionally               = 4,
    LCSectionPublicationAndAssistance   = 5,
    LCSectionButton                     = 6
    
};

@interface LCCalculatorController : UITableViewController

@end
