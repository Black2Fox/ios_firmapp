//
//  LCNoConnectionStatusCell.m
//  Vizitka_2
//
//  Created by Vio on 02.12.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCNoConnectionStatusCell.h"

@implementation LCNoConnectionStatusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionReloadController:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(actionReloadController:)]) {
        
        [self.delegate actionReloadController:sender];
    }
    
}
@end
