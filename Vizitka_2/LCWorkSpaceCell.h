//
//  LCWorkSpaceCell.h
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCWorkSpaceCell : UITableViewCell <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (weak, nonatomic) IBOutlet UIView *segmentBarView;

- (IBAction)actionSegmentControl:(id)sender;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

@property (weak, nonatomic) IBOutlet UIView *mainWorkStudio;
@property (assign, nonatomic) CGFloat sizeWidhtWindow;
@property (assign, nonatomic) CGFloat sizeHightWindow;
@end
