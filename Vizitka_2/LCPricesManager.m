//
//  LCPricesManager.m
//  Vizitka_2
//
//  Created by Vio on 25.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCPricesManager.h"
#import "Backendless.h"

@implementation LCPricesManager

+ (LCPricesManager *)platformIOS:(NSString *)iOs platformAndroid:(NSString *)android devicePhone:(NSString *)phone
                    deviceTablet:(NSString *)tablet deviceWatch:(NSString *)watch
                     authCabinet:(NSString *)cabinet authSocial:(NSString *)social
                     addictCloud:(NSString *)cloud addictInApp:(NSString *)inApp addictTeamWork:(NSString *)teamWork addictPush:(NSString *)push
          publAndSettingAppstore:(NSString *)appstore publAndSettingDeeplink:(NSString *)deeplink publAndSettingSearch:(NSString *)search mainLabel:(NSString *)label{
    
    LCPricesManager* price = [LCPricesManager new];
    
    price.platformIOS = iOs;
    price.platformAndroid = android;
    
    price.devicePhone = phone;
    price.deviceTablet = tablet;
    price.deviceWatch = watch;
    
    price.authCabinet = cabinet;
    price.authSocial = social;
    
    price.addictCloud = cloud;
    price.addictInApp = inApp;
    price.addictTeamWork = teamWork;
    price.addictPush = push;
    
    price.publAndSettingAppstore = appstore;
    price.publAndSettingDeeplink = deeplink;
    price.publAndSettingSearch = search;
    
    price.generalLabel = label;
    
    return price;
}

+ (NSArray *)downloadPriceFromServer {
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[LCPricesManager class]];
    BackendlessCollection *collection = [dataStore find:nil];
    
    if (collection.data == nil) {
        NSArray* array = [NSArray new];
        return array;
    } else {
        return collection.data;
    }
    
}

@end
