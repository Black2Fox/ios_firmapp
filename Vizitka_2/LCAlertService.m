//
//  LCAlertService.m
//  Vizitka_2
//
//  Created by Vio on 25.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCAlertService.h"
@interface LCAlertService ()

@end

@implementation LCAlertService

- (void)showAlertButtonOkWithTitle:(NSString *)title andMessage:(NSString *)message inVC:(UIViewController *)view {
    
    UIAlertController* theAlert = [UIAlertController alertControllerWithTitle:title
                                                                      message:message
                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [theAlert addAction:ok];

    [view presentViewController:theAlert animated:YES completion:nil];
}

#pragma mark - Initial Methods

- (instancetype)initPrivate {
    return [super init];
}

+ (instancetype)instance {
    static LCAlertService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] initPrivate];
    });
    return instance;
}

- (void)showAlertSkypeInVC:(UIViewController *)view {
    
    UIAlertController* theAlert = [UIAlertController alertControllerWithTitle:kMissedSkypeAgent message:kDownloadMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* actionOne = [UIAlertAction actionWithTitle:@"Да" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://appsto.re/ru/Uobls.i"]];
    }];
    
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"Нет" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [theAlert addAction:actionOne];
    [theAlert addAction:actionCancel];
    
    
    [view presentViewController:theAlert animated:YES completion:nil];

}

- (void)showAlertAppStoreInVC:(UIViewController *)view andLink:(NSURL *)link {
    
    UIAlertController* theAlert = [UIAlertController alertControllerWithTitle:kWarning message:kAppStore preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* actionOne = [UIAlertAction actionWithTitle:@"Да" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [[UIApplication sharedApplication] openURL:link];
    }];
    
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"Нет" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [theAlert addAction:actionOne];
    [theAlert addAction:actionCancel];
    
    
    [view presentViewController:theAlert animated:YES completion:nil];
    
}

@end
