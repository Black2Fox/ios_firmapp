//
//  LCSocialImageCell.m
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCSocialImageCell.h"

@implementation LCSocialImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CGFloat heightAndWidth = 44.f;
    CGFloat halfHeightAndWidth = heightAndWidth / 2;
    
    CGFloat anywayY = 2.f;
    
    CGFloat widthWindow = self.contentView.frame.size.width;
    CGFloat sizeSection = widthWindow / 4;
    
    CGFloat startSeparate = (sizeSection - heightAndWidth) / 2;
    CGFloat startPosition = startSeparate + halfHeightAndWidth - 1;
    
    CGFloat startX2 = sizeSection / 1.2f;
    
    self.buttonBe.frame = CGRectMake(startPosition, anywayY, heightAndWidth, heightAndWidth);
    self.buttonIn.frame = CGRectMake(startPosition + startX2, anywayY, heightAndWidth, heightAndWidth);
    self.buttonFb.frame = CGRectMake(startPosition + startX2 * 2, anywayY, heightAndWidth, heightAndWidth);
    self.buttonTwitter.frame = CGRectMake(startPosition + startX2 * 3, anywayY, heightAndWidth, heightAndWidth);

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionBe:(UIButton *)sender {
}

- (IBAction)actionIn:(UIButton *)sender {
    
}
- (IBAction)actionFb:(UIButton *)sender {
    
}
- (IBAction)actionTwitter:(UIButton *)sender {
    
}
@end
