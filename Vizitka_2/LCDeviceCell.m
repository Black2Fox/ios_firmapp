//
//  LCDeviceCell.m
//  Vizitka_2
//
//  Created by Vio on 22.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCDeviceCell.h"

@implementation LCDeviceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIImage* normalOvel = [UIImage imageNamed:@"oval_back"];
    UIImage* selectedOval = [UIImage imageNamed:@"oval_high"];
    
    [self.iOSButton setImage:normalOvel forState:UIControlStateNormal];
    [self.iOSButton setImage:selectedOval forState:UIControlStateSelected];
    
    [self.androidButton setImage:normalOvel forState:UIControlStateNormal];
    [self.androidButton setImage:selectedOval forState:UIControlStateSelected];
    
    [self.androidButton addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.iOSButton addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)buttonTouch:(UIButton *)aButton withEvent:(UIEvent *)event {
    aButton.selected = !aButton.selected;
   
}

@end
