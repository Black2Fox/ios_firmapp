//
//  LCWorkSpaceConstantClass.h
//  Vizitka_2
//
//  Created by Vio on 09.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString* const kMainMessageSecondWindow = @"Выбрав компанию NoName в качестве разработчика Вашего приложения, Вы получите:";
static NSString* const kReportText = @"еженедельные отчеты";
static NSString* const kTermsText = @"точное соответствие срокам";
static NSString* const kFeedbackText = @"оперативная обратная связь";
static NSString* const kSafeGuardText = @"гарантийное обслуживание";//после релиза


static NSString* const kMainMessageFirstWindow = @"Компания NoName предлагает новый подход к разработке мобильных приложений.";
static NSString* const kTwoMainMessageFirstWindow = @"За время работы над нашим первым проектом - inSearchApp - мы сформировали правила, которых придерживаемся в работе:";

static NSString* const kItem1 = @"•   Oтветственность";
static NSString* const kItem2 = @"•   Открытость";
static NSString* const kItem3 = @"•   Готовность постоянно учиться";
static NSString* const kItem4 = @"•   Что то еще";

static NSString* const kBaseMentWindows = @"Мы придерживаемся принципов ROWE и оцениваем по результатам работы, а не месту и способу ее выполнения.";

