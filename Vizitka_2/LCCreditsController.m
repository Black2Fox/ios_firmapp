//
//  LCCreditsController.m
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCCreditsController.h"

#import "LCWorkSpaceCell.h"
#import "LCLowGroundContent.h"

#import "LCWebView.h"
#import "UIButton+LCCustomProperty.h"
#import "LCLowGHelper.h"

static NSString* const kIdentifireWorkSpaceCell = @"workSpaceCell";
static NSString* const kIdentifireLowGroundCell = @"lowGroundCell";
static NSString* const kIdentifireNameCompany = @"Modoapp";
static NSString* const kIdentifireWebView = @"LCWebView";

@interface LCCreditsController () <CustomLowgroundDelegate>

@property (assign, nonatomic) CGFloat makeSizeWindow;
@property (assign, nonatomic) BOOL connection;
@property (strong, nonatomic) LCLowGHelper* helper;

@end


@implementation LCCreditsController

- (void)loadView {
    [super loadView];
    
    self.connection = [LCConnectionStatus statusConnected];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.makeSizeWindow = self.tableView.frame.size.height - 18 - 44 - 44;
      
    [self.tableView registerNib:[UINib nibWithNibName:@"LCWorkSpaceCell" bundle:nil] forCellReuseIdentifier:kIdentifireWorkSpaceCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCLowGroundContent" bundle:nil] forCellReuseIdentifier:kIdentifireLowGroundCell];
    
    self.navigationItem.title = kIdentifireNameCompany;
    
    if (self.connection == NO) {
        [self presentViewController:[LCConnectionStatus noInternetConnection]  animated:YES completion:nil];
    } 
    
    self.helper.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        
    if (indexPath.section == 0) {
        
        LCWorkSpaceCell* cell = (LCWorkSpaceCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireWorkSpaceCell];
        
        if (!cell) {
            cell = [[LCWorkSpaceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireWorkSpaceCell];
        }
        
        return cell;
        
    }

    if (indexPath.section == 1) {
        
        LCLowGroundContent* cell = (LCLowGroundContent *)[tableView dequeueReusableCellWithIdentifier:kIdentifireLowGroundCell];
        if (!cell) {
            cell = [[LCLowGroundContent alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireLowGroundCell];
        }
        
        [cell.contentView setTintColor:[UIColor applicationMainBackgroundColor]];
        cell.internetStatus = self.connection;
        
        return cell;
    }
    
    else {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"123"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"123"];
        }
        
        return cell;
    }
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case 0:
            return self.makeSizeWindow - 160.f;
            
        case 1:
            return 160.f;
            
        default:
            break;
    }
    
    return 44.f;
}

- (void)tabButtonWithParameter:(UIButton *)sender {
    
    LCWebView *vc = [self.storyboard instantiateViewControllerWithIdentifier:kIdentifireWebView];
    
    vc.urlData = sender.customUrlValue;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

@end
