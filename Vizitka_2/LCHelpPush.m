//
//  LCHelpPush.m
//  Vizitka_2
//
//  Created by Vio on 22.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCHelpPush.h"

@implementation LCHelpPush

+ (void)settingPriceCell:(LCMainPriceCell *)cell {
    
    cell.mainViewContent.backgroundColor = [UIColor applicationMainViewColor];
    cell.mainViewContent.layer.cornerRadius = 5.f;
    cell.mainViewContent.layer.masksToBounds = YES;
    
    cell.addictUserLabel.textColor = [UIColor applicationTextCalcColor];
    
}
+ (void)settingCallbackCell:(LCCallbackCell *)cell {
    
    cell.topLabel.textColor = [UIColor applicationCalcLabelColor];
    cell.botLabel.textColor = [UIColor applicationCalcLabelColor];
    
}
+ (void)settingNumberAndEmailCell:(LCNumberAndEmailCell *)cell {
    
    cell.phoneLabel.textColor = [UIColor applicationLabelColor];
    cell.mailLabel.textColor = [UIColor applicationLabelColor];
    
    cell.buttonSend.backgroundColor = [UIColor applicationCallMeButtonColor];
    [cell.buttonSend setTintColor:[UIColor applicationCallMeLabelColor]];
    
    cell.buttonSend.layer.masksToBounds = YES;
    cell.buttonSend.layer.cornerRadius = 5.f;
    
    
}


@end
