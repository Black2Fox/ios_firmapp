//
//  LCPortfolioController.m
//  Vizitka_2
//
//  Created by Vio on 09.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

//размер для блоков
/*
  2 row first pic 750x977 (0.767)
  2 row second pic 748x1215 (0.615)
*/
#import "LCPortfolioController.h"
#import "LCCreditsController.h"
#import "LCPortfolioDataDownload.h"
#import "UIImageView+WebCache.h"
#import "LCAlertService.h"

#import "UIButton+LCCustomProperty.h"
#import "MXSegmentedPager.h"

#import "LCPortfolioConst.h"
#import "LCConnectionStatus.h"

#import "Backendless.h"

static NSString* const kDefaultTitleLeft = @"О нас";

@interface LCPortfolioController () <MXSegmentedPagerDelegate, MXSegmentedPagerDataSource, UITableViewDelegate, UITableViewDataSource, CAAnimationDelegate, LCNoConnectionDelegate, DownloadButtonDelegate>

@property (nonatomic, strong) MXSegmentedPager  * segmentedPager;
@property (nonatomic, strong) NSMutableArray    * tableViewArray;
@property (assign, nonatomic) CGFloat widthWindow;
@property (assign, nonatomic) CGFloat percentSize;

@property (assign, nonatomic) BOOL isAppear;
@property (assign, nonatomic) BOOL internetStatus;
@property (assign, nonatomic) BOOL checkerCount;
@property (assign, nonatomic) BOOL needReload;

@property (strong, nonatomic) NSMutableArray* arrayLinks;

@end

@implementation LCPortfolioController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isAppear = YES;
    
    //[backendless.persistenceService save:[LCPortfolioDataDownload hello:@"google.com"]];
    
    [self prepareController];
    [self checkArrayWithCount:self.arrayContent];
    [self makeSortingAndPrepareArray];
    
    self.navigationController.navigationBar.tintColor = [UIColor applicationBarColor];
    
    [self createButtons];
}

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectView:(UIView *)view {
    
    //метод который вызывается при смене контроллера
    
}


- (void)makeSortingAndPrepareArray {
    
    if (self.checkerCount == YES) {

        self.arrayContent = [self.arrayContent sortedArrayUsingComparator: ^(LCPortfolioDataDownload* obj1, LCPortfolioDataDownload* obj2) {
            
            if ([obj1.serialNumber integerValue] > [obj2.serialNumber integerValue]) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            
            if ([obj1.serialNumber integerValue] < [obj2.serialNumber integerValue]) {
                return (NSComparisonResult)NSOrderedAscending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }];
        
    }
    
    
    self.tableViewArray = [NSMutableArray new];
    
    for (NSInteger i = 0; i < [self.arrayContent count]; i++) {
        UITableView* tb = [self createTable];
        [self.tableViewArray addObject:tb];
    }
    
    if (self.needReload == YES) {
        
        for (NSInteger i = 0; i < [self.tableViewArray count]; i++) {
            UITableView* view = [self.tableViewArray objectAtIndex:i];
            //[self.segmentedPager reloadData];
            [view reloadData];
        }
    }
}

- (UITableView *)createTable {
    
    UITableView* tb = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    tb.delegate = self;
    tb.dataSource = self;
    tb.allowsSelection = NO;
    tb.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tb setContentInset:UIEdgeInsetsMake(0, 0, 90, 0)];

    return tb;
    
}

- (void)prepareController {
    
    [self.view addSubview:self.segmentedPager];
    self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationUp;
    
    self.segmentedPager.segmentedControl.selectionIndicatorColor = [UIColor applicationBarColor];
    //его ширина
    self.segmentedPager.segmentedControl.selectionIndicatorHeight = 4.f;
    
    self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedPager.segmentedControl.titleTextAttributes =
    @{NSFontAttributeName : [UIFont systemFontOfSize:14]};
    self.segmentedPager.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
          NSFontAttributeName : [UIFont systemFontOfSize:14]};
    
    self.segmentedPager.pager.gutterWidth = CGFLOAT_MIN;
    
    self.arrayLinks = [NSMutableArray new];
    
    self.internetStatus = [LCConnectionStatus statusConnected];
    if (self.internetStatus == YES){
        self.arrayContent = [LCPortfolioDataDownload downloadContentFromServer];
        
    } else {
        self.arrayContent = [NSArray arrayWithObject:@"Clear"];
    }
}

- (void)viewWillLayoutSubviews {
    
    self.widthWindow = self.view.frame.size.width;
    
    if (self.isAppear == YES) {
        
        self.segmentedPager.frame = (CGRect){
            .origin.x       = 0.f,
            .origin.y       = 63.f,
            .size.width     = self.view.frame.size.width,
            .size.height    = self.view.frame.size.height - 13.f
        };
        
        [self prepareSize];
        self.isAppear = NO;
    }
    
    
    
   [super viewWillLayoutSubviews];
    
}

- (void)prepareSize {
    
    //ширина экрана
    CGFloat widthWindow = self.widthWindow;
    
    //узнаем увеличение
    
    if (self.widthWindow == 320) {
        self.percentSize = 1.f;
    } else if (widthWindow == 375) {
        self.percentSize = 1.17f;
    } else if (widthWindow == 414) {
        self.percentSize = 1.3f;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)createButtons {
    
    UIBarButtonItem* rightItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                               target:self
                                                                               action:@selector(actionShare:)];
    
    
    [self.navigationItem setRightBarButtonItem:rightItem];
    
}

#pragma mark - Properties

- (MXSegmentedPager *)segmentedPager {
    if (!_segmentedPager) {
        _segmentedPager = [[MXSegmentedPager alloc] init];
        _segmentedPager.delegate    = self;
        _segmentedPager.dataSource  = self;
    }
    return _segmentedPager;
}

#pragma mark - MXSegmentedPagerDelegate

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectViewWithTitle:(NSString *)title {

}

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectViewWithIndex:(NSInteger)index {
    
    LCPortfolioDataDownload* down = [self.arrayContent objectAtIndex:index];
    segmentedPager.segmentedControl.backgroundColor = [self colorAtIndex:down];
    self.view.backgroundColor = [self colorAtIndex:down];
    [self viewSlideInFromRightToLeft:segmentedPager.segmentedControl];
    
}

-(void)viewSlideInFromRightToLeft:(UIView *)views
{
    CATransition *transition = nil;
    transition = [CATransition animation];
    transition.duration = 0.3f;//kAnimationDuration
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromTop;
    transition.delegate = self;
    [views.layer addAnimation:transition forKey:nil];
}

#pragma mark - MXSegmentedPagerDataSource

- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return [self.arrayContent count];
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index {
    
    if (self.checkerCount == YES) {
        LCPortfolioDataDownload* down = [self.arrayContent objectAtIndex:index];
        
        return down.nameProject;
    } else {
       return @"Нет соединения";
    }
    
}

- (UIView *)segmentedPager:(MXSegmentedPager *)segmentedPager viewForPageAtIndex:(NSInteger)index {
    
    return self.tableViewArray[index];
}

#pragma mark - UITableViewDelegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.checkerCount == YES) {
        for (NSInteger i = 0; i < [self.tableViewArray count]; i++) {
            
            if (tableView == [self.tableViewArray objectAtIndex:i]) {
                
                LCPortfolioDataDownload* down = [self.arrayContent objectAtIndex:i];
                if (down.count == 2) {
                    switch (indexPath.row) {
                        case 0:
                            return 480 * self.percentSize;
                            
                        case 1:
                            return 520 * self.percentSize;
                    }
                }
                if (down.count == 3) {
                    switch (indexPath.row) {
                        case 0:
                            return 480.f;
                            
                        case 1:
                            return 250 * self.percentSize;
                      
                        case 2:
                            return 580.f;
                    }
                }
            }
        }

    }
    
    return self.view.frame.size.height - (20 + 44 + 49 + 44);
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.checkerCount == YES) {
        for (NSInteger i = 0; i < [self.tableViewArray count]; i++) {
            
            if (tableView == [self.tableViewArray objectAtIndex:i]) {
                LCPortfolioDataDownload* down = [self.arrayContent objectAtIndex:i];
                return down.count;
            }
        }
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier2 = @"Cell2";
    [tableView registerNib:[UINib nibWithNibName:@"LCAAPhotoCell" bundle:nil] forCellReuseIdentifier:kIdentifireAAPhotoCell];
    [tableView registerNib:[UINib nibWithNibName:@"LCABLabelCell" bundle:nil] forCellReuseIdentifier:kIdentifireABLabelCell];
    [tableView registerNib:[UINib nibWithNibName:@"LCABPhotoCell" bundle:nil] forCellReuseIdentifier:kIdentifireABPhotoCell];
    [tableView registerNib:[UINib nibWithNibName:@"LCNoConnectionStatusCell" bundle:nil] forCellReuseIdentifier:kIdentifireNoConnectionCell];
    
    if (self.checkerCount == YES) {
        for (NSInteger i = 0; i < [self.tableViewArray count]; i++) {
    
            if (tableView == [self.tableViewArray objectAtIndex:i]) {
                
                LCPortfolioDataDownload* down = [self.arrayContent objectAtIndex:i];
                if (down.count == 2) {
                    tableView.backgroundColor = [self colorAtIndex:down];
                    if (indexPath.row == 0) {
                        LCAAPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifireAAPhotoCell];
                        if (!cell) {
                            cell = [[LCAAPhotoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireAAPhotoCell];
                        }
                        cell.labelFirstWindow.text = down.labelOne;
                        NSURL* urlString = [NSURL URLWithString:down.imageURLOne];
                       
                        [cell.aaPhoto sd_setImageWithPreviousCachedImageWithURL:urlString placeholderImage:nil options:SDWebImageHandleCookies progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                            
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            
                        }];
                        cell.delegate = self;
                        
                        NSURL* urlDownload = [NSURL URLWithString:down.urlDownloadApp];
                        cell.downloadButton.customUrlValue = urlDownload;
                        
                        return cell;
                    }
                    
                    if (indexPath.row == 1) {
                        
                        LCAAPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifireAAPhotoCell];
                        if (!cell) {
                            cell = [[LCAAPhotoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireAAPhotoCell];
                        }
                        NSURL* urlString = [NSURL URLWithString:down.imageURLTwo];
                        cell.labelFirstWindow.text = @"";
                        [cell.aaPhoto sd_setImageWithPreviousCachedImageWithURL:urlString placeholderImage:nil options:SDWebImageHandleCookies progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                            
                        }  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            
                        }];
                        
                        cell.downloadButton.hidden = YES;
                        
                        return cell;
                        
                    }
                    
                }
                if (down.count == 3) {
                    
                    tableView.backgroundColor = [self colorAtIndex:down];
                    
                    if (indexPath.row == 0) {
                        
                        LCAAPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifireAAPhotoCell];
                        if (!cell) {
                            cell = [[LCAAPhotoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireAAPhotoCell];
                        }
                        NSURL* urlString = [NSURL URLWithString:down.imageURLOne];
                        cell.labelFirstWindow.text = down.labelOne;
                        [cell.aaPhoto sd_setImageWithPreviousCachedImageWithURL:urlString placeholderImage:nil options:SDWebImageHandleCookies progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                            
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        }];
                        
                        cell.delegate = self;
                        
                        NSURL* urlDownload = [NSURL URLWithString:down.urlDownloadApp];
                        cell.downloadButton.customUrlValue = urlDownload;
                        
                        return cell;
                        
                    }

                    if (indexPath.row == 1) {
                        
                        LCAAPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifireAAPhotoCell];
                        if (!cell) {
                            cell = [[LCAAPhotoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireAAPhotoCell];
                        }
                        NSURL* urlString = [NSURL URLWithString:down.imageURLTwo];
                        cell.labelFirstWindow.text = @"";
                        [cell.aaPhoto sd_setImageWithPreviousCachedImageWithURL:urlString placeholderImage:nil options:SDWebImageHandleCookies progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                            
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        }];
                        
                        cell.downloadButton.hidden = YES;
                        
                        return cell;
                        
                    }

                    if (indexPath.row == 2) {
                        
                        LCABPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifireABPhotoCell];
                        if (!cell) {
                            cell = [[LCABPhotoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireABPhotoCell];
                        }
                        NSURL* urlString = [NSURL URLWithString:down.imageURLThree];
                        cell.topLabel.text = down.labelTwo;
                        [cell.abImage sd_setImageWithPreviousCachedImageWithURL:urlString placeholderImage:nil options:SDWebImageHandleCookies progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                            
                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        }];
                        
                        
                        return cell;
                        
                    }
                    
                    
                    
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
                    if (cell == nil) {
                        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2];
                    }
                    
                    if (indexPath.row == 0) {
                        cell.textLabel.text = @"ZERO HELLO";
                    } else {
                        cell.textLabel.text = [NSString stringWithFormat:@"%ld", (long)down.count];
                    }
                    
                    cell.backgroundColor = [UIColor yellowColor];
                    
                    return cell;
                    
                }
                
            }
            
        }

    }
    
    if (self.checkerCount == NO) {
        LCNoConnectionStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifireNoConnectionCell];
        if (!cell) {
            cell = [[LCNoConnectionStatusCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireNoConnectionCell];
        }
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.view.bounds;
        gradient.colors = @[(id)[UIColor applicationLabelColor].CGColor,
                            (id)[UIColor applicationMainBackgroundColor].CGColor];
        [cell.contentView.layer addSublayer:gradient];
        cell.labelStatus.text = @"Проверьте ваше подключение и повторите";
        cell.buttonStatus.titleLabel.text = @"Повторить";
        [cell addSubview:cell.labelStatus];
        [cell addSubview:cell.buttonStatus];
        
        cell.delegate = self;
        
        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cc"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cc"];
    }
    cell.textLabel.text = @"default";
    
    cell.backgroundColor = [UIColor yellowColor];
    
    return cell;
    
    
}
#pragma mark - ColorAtIndex
- (UIColor *)colorAtIndex:(LCPortfolioDataDownload *)object {
    
    if (self.checkerCount == YES) {
        UIColor* colorOne = [UIColor colorWithRed:object.red/255.f green:object.green/255.f blue:object.blue/255.f alpha:1];
        return colorOne;
    } else {
        UIColor* colorTwo = [UIColor darkGrayColor];
        return colorTwo;
    }
}

- (void)checkArrayWithCount:(NSArray *)array {
    
    if (array.count > 1) {
        self.checkerCount = YES;
    } else {
        self.checkerCount = NO;
    }
    
}

- (IBAction)actionShare:(id)sender {
    
    NSString * title = @"http://insearchapp.ru";
    NSArray* dataToShare = @[title];
    UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypeMessage];
    [self presentViewController:activityViewController animated:YES completion:^{}];
    
}


- (IBAction)downloadAction:(UIButton *)sender {
    
    [[LCAlertService instance] showAlertAppStoreInVC:self andLink:sender.customUrlValue];
    
}

@end

