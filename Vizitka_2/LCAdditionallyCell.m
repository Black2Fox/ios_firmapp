//
//  LCAdditionallyCell.m
//  Vizitka_2
//
//  Created by Vio on 23.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCAdditionallyCell.h"

@implementation LCAdditionallyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIImage* normalOvel = [UIImage imageNamed:@"oval_back"];
    UIImage* selectedOval = [UIImage imageNamed:@"oval_high"];
    
    [self.buttonCloud setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonCloud setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonPush setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonPush setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonInApp setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonInApp setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonTeamWork setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonTeamWork setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonCloud addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buttonPush addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonInApp addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonTeamWork addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)buttonTouch:(UIButton *)aButton withEvent:(UIEvent *)event {
    aButton.selected = !aButton.selected;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
