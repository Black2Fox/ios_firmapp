//
//  LCUserDataManager.h
//  Vizitka_2
//
//  Created by Vio on 20.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LCUserDataManager : NSObject

//PhoneBlock
@property (strong, nonatomic) NSString* contactPhone;
@property (strong, nonatomic) NSString* contactName;

//TimeBlock
@property (strong, nonatomic) NSString* suitebleTime;

//AttachBlock
@property (strong, nonatomic) NSString* attachmentText;


+ (LCUserDataManager *)sendToServerUserInfoContactPhone:(NSString *)contactPhone andContactName:(NSString *)contactName andTime:(NSString *)time andAttachment:(NSString *)attachment;

@end
