//
//  LCABPhotoCell.h
//  Vizitka_2
//
//  Created by Vio on 05.12.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCABPhotoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIImageView *abImage;

@end
