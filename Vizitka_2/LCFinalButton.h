//
//  LCFinalButton.h
//  Vizitka_2
//
//  Created by Vio on 25.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CalculateAllDataDelegate;

@interface LCFinalButton : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *buttonFinal;

@property (nonatomic) id<CalculateAllDataDelegate> delegate;

@end

@protocol CalculateAllDataDelegate <NSObject>

@optional

- (IBAction)finalAction:(UIButton *)sender;

@end
