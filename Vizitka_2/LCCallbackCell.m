//
//  LCCallbackCell.m
//  Vizitka_2
//
//  Created by Vio on 14.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCCallbackCell.h"

@interface LCCallbackCell ()

@property (assign, nonatomic) CGFloat widthWindow;
@property (assign, nonatomic) BOOL isAppear;

@end

@implementation LCCallbackCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.isAppear = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.widthWindow = self.contentView.frame.size.width;
    
    if (self.isAppear) {
        [self prepareCell];
    }
}


- (void) prepareCell {
    
    CGFloat percentSize;
    if (self.widthWindow == 320) {
        percentSize = 1.f;
    } else if (self.widthWindow == 375) {
        percentSize = 1.17f;
    } else if (self.widthWindow == 414) {
        percentSize = 1.3f;
    }
    
    [self.topLabel setFont:[UIFont systemFontOfSize:11 * percentSize]];
    [self.botLabel setFont:[UIFont systemFontOfSize:11 * percentSize]];
    
    self.isAppear = NO;
    
}

- (IBAction)actionCall:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt:+79009009090"]];
    
}

- (IBAction)actionEmail:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(actionEmail:)]) {
        
        [self.delegate actionEmail:sender];
    }
    
}

@end
