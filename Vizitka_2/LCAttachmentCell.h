//
//  LCAttachmentCell.h
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AttachmentDelegate;
@protocol CollectionAllDataDelegate;

@interface LCAttachmentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *attachmentLabel;
@property (weak, nonatomic) IBOutlet UITextView *attachTextView;
@property (weak, nonatomic) IBOutlet UIButton *buttonCollect;
@property (weak, nonatomic) IBOutlet UIButton *attachButton;

@property (nonatomic) id<AttachmentDelegate> delegate;
@property (nonatomic) id<CollectionAllDataDelegate> delegateCollection;
@end

@protocol AttachmentDelegate <NSObject>

@optional

- (IBAction)actionPushAttachment:(id)sender;

@end

@protocol CollectionAllDataDelegate <NSObject>

@optional

- (IBAction)actionCollectData:(id)sender;

@end
