//
//  LCPricesManager.h
//  Vizitka_2
//
//  Created by Vio on 25.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LCPricesManager : NSObject

#pragma mark - Platform

@property (strong, nonatomic) NSString* platformIOS;
@property (strong, nonatomic) NSString* platformAndroid;

#pragma mark - Device

@property (strong, nonatomic) NSString* devicePhone;
@property (strong, nonatomic) NSString* deviceTablet;
@property (strong, nonatomic) NSString* deviceWatch;

#pragma mark - Auth

@property (strong, nonatomic) NSString* authCabinet;
@property (strong, nonatomic) NSString* authSocial;

#pragma mark - Addict

@property (strong, nonatomic) NSString* addictCloud;
@property (strong, nonatomic) NSString* addictInApp;
@property (strong, nonatomic) NSString* addictTeamWork;
@property (strong, nonatomic) NSString* addictPush;

#pragma mark - PublAndSetting

@property (strong, nonatomic) NSString* publAndSettingAppstore;
@property (strong, nonatomic) NSString* publAndSettingDeeplink;
@property (strong, nonatomic) NSString* publAndSettingSearch;

#pragma mark - mainLabel

@property (strong, nonatomic) NSString* generalLabel;

+ (LCPricesManager *)platformIOS:(NSString *)iOs
                 platformAndroid:(NSString *)android

                     devicePhone:(NSString *)phone
                    deviceTablet:(NSString *)tablet
                     deviceWatch:(NSString *)watch

                     authCabinet:(NSString *)cabinet
                      authSocial:(NSString *)social

                     addictCloud:(NSString *)cloud
                     addictInApp:(NSString *)inApp
                  addictTeamWork:(NSString *)teamWork
                      addictPush:(NSString *)push

          publAndSettingAppstore:(NSString *)appstore
          publAndSettingDeeplink:(NSString *)deeplink
            publAndSettingSearch:(NSString *)search

                       mainLabel:(NSString *)label;

+ (NSArray *)downloadPriceFromServer;

@end
