//
//  LCLowGroundContent.m
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCLowGroundContent.h"
#import "LCDownloadImage.h"
#import "LCWebView.h"
#import "UIButton+LCCustomProperty.h"
#import "LCLowGHelper.h"
#import "UIImageView+WebCache.h"

@interface LCLowGroundContent () <UIScrollViewDelegate>

@property (assign, nonatomic) BOOL isAppear;
@property (strong, nonatomic) NSArray* arrayImage;

@end

@implementation LCLowGroundContent

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.isAppear = YES;
    self.pageControl.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.sizeHightWindow = self.contentView.frame.size.height;
    self.sizeWidhtWindow = self.contentView.frame.size.width;
    
    if (self.isAppear) {
        
        @try {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (self.internetStatus == YES) {
                        self.arrayImage = [LCDownloadImage downloadImageFromServer];
                        [self prepareCell];
                    }
                    
                });
            });
        } @catch (NSException *exception) {
            NSLog(@"не упал))00");
        } @finally {
            
        }
    }
}

- (void)prepareCell {
   //индикатор
    
    self.pageControl.numberOfPages = [self.arrayImage count];
    
    
    //ширина экрана
    CGFloat widthWindow = self.sizeWidhtWindow;
    
    //узнаем увеличение
    
    CGFloat percentSize = 0.0;
    if (widthWindow == 320) {
        percentSize = 1.f;
    } else if (widthWindow == 375) {
        percentSize = 1.17f;
    } else if (widthWindow == 414) {
        percentSize = 1.3f;
    }
    
    //размеры вью на скроле
    CGFloat height = self.sizeHightWindow;
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, widthWindow * [self.arrayImage count], height)];
    
    view.backgroundColor = [UIColor clearColor];
    
    NSInteger i = 0;
    
    
    for (LCDownloadImage* image in self.arrayImage) {
        
        self.scrollMainView.frame = CGRectMake(0, 0, self.sizeWidhtWindow, self.sizeHightWindow);
        self.activityIndicator.center = CGPointMake(self.sizeWidhtWindow / 2.0, self.sizeHightWindow / 2.0);
        
        
        NSURL* urlImage = [NSURL URLWithString:image.imageURL];
        NSURL* urlButton = [NSURL URLWithString:image.linkButton];
        //[self.activityIndicator startAnimating];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self download:i andUrl:urlImage view:view andLabelOne:image.labelOne andLabelTwo:image.labelTwo andLinkButton:urlButton andLabelButton:image.labelButton andPercent:percentSize];
            
        });
        
        i++;
    }
    
    self.scrollMainView.backgroundColor = [UIColor clearColor];
    self.scrollMainView.showsHorizontalScrollIndicator = NO;
    self.scrollMainView.pagingEnabled = YES;
    [self.scrollMainView setDelegate:self];
    [self.scrollMainView setShowsVerticalScrollIndicator:NO];
    [self.scrollMainView setShowsHorizontalScrollIndicator:NO];
    [self.scrollMainView addSubview:view];
    [self.contentView addSubview:self.pageControl];
    
    self.scrollMainView.contentSize = CGSizeMake(view.frame.size.width, self.scrollMainView.frame.size.height);
    
    self.isAppear = NO;

  // [self.activityIndicator stopAnimating];
    
    
}
- (void)download:(NSInteger)i andUrl:(NSURL *)urlString view:(UIView *)view andLabelOne:(NSString *)labelOne andLabelTwo:(NSString *)labelTwo
   andLinkButton:(NSURL *)linkButton andLabelButton:(NSString *)labelButton andPercent:(CGFloat)percent{
    
    
    UIImageView* imageRect = [[UIImageView alloc] initWithFrame:CGRectMake(0 + (self.sizeWidhtWindow * i), 0, self.sizeWidhtWindow, self.sizeHightWindow)];
    imageRect.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    
    
    CGRect rectFirstLabel = CGRectMake((self.sizeWidhtWindow * i), self.sizeHightWindow / 8, self.sizeWidhtWindow, 40);
    
    CGFloat secondLabelY = self.sizeHightWindow / 2.5f;
    
    CGRect rectSecondLabel = CGRectMake(self.sizeWidhtWindow / 2 - self.sizeHightWindow / 6 + (self.sizeWidhtWindow * i),secondLabelY , self.sizeWidhtWindow / 1.9f, 60);
    
    CGRect rectButton = CGRectMake(self.sizeWidhtWindow * i + 25, secondLabelY + 15, 100 * percent, 35);
    
    self.activityIndicator.hidesWhenStopped = YES;
    [self.activityIndicator startAnimating];
    [self.scrollMainView addSubview:self.activityIndicator];
    
    
    [imageRect sd_setImageWithPreviousCachedImageWithURL:urlString placeholderImage:nil options:SDWebImageHandleCookies progress:^(NSInteger receivedSize, NSInteger expectedSize) {
    
        [self.activityIndicator removeFromSuperview];
    }
        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
    {
        [self.activityIndicator removeFromSuperview];
    }];
    
    
    
    //[self viewSlideInFromRightToLeft:view];
    [view addSubview:imageRect];
    [view addSubview:[LCLowGHelper settingFirstLabel:rectFirstLabel andText:labelOne andPercent:percent]];
    [view addSubview:[LCLowGHelper settingSecondLabel:rectSecondLabel andText:labelTwo andPercent:percent]];
    LCLowGHelper* helperButton = [LCLowGHelper new];
    [view addSubview:[helperButton settingButton:rectButton andText:labelButton andPercent:percent andURL:linkButton]];
    self.pageControl.hidden = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}

-(void)viewSlideInFromRightToLeft:(UIView *)views
{
    CATransition *transition = nil;
    transition = [CATransition animation];
    transition.duration = 1;//kAnimationDuration
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    transition.type = kCATransitionPush;
    transition.subtype =kCATransitionFromRight;
    transition.delegate = self;
    [views.layer addAnimation:transition forKey:nil];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger page = scrollView.contentOffset.x/scrollView.frame.size.width;
    self.pageControl.currentPage=page;
}
- (IBAction)actionPageControl:(id)sender {
    
    NSInteger page = (int)self.pageControl.currentPage;
    CGRect frame = self.scrollMainView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.scrollMainView scrollRectToVisible:frame animated:YES];
}

@end
