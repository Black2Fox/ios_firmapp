//
//  LCCalculatorController.m
//  Vizitka_2
//
//  Created by Vio on 14.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCCalculatorController.h"

#import "LCCalcFirstTextCell.h"
#import "LCDeviceCell.h"
#import "LCPlatformDeviceCell.h"
#import "LCAuthCell.h"
#import "LCAdditionallyCell.h"
#import "LCPublicationAndAssistanceCell.h"
#import "LCFinalButton.h"

#import "LCPricesManager.h"

#import "LCCalculatorPush.h"

#import "UIButton+LCCustomProperty.h"
#import "LCAlertService.h"

@interface LCCalculatorController () <CalculateAllDataDelegate>

@property (strong, nonatomic) NSArray* priceArray;
//Клоны
@property (weak, nonatomic) UIButton* buttoniOS;
@property (weak, nonatomic) UIButton* buttonAndroid;

@property (weak, nonatomic) UIButton* buttonDevicePhone;
@property (weak, nonatomic) UIButton* buttonDeviceTablet;
@property (weak, nonatomic) UIButton* buttonDeviceWatch;

@property (weak, nonatomic) UIButton* buttonCabinet;
@property (weak, nonatomic) UIButton* buttonSocial;

@property (weak, nonatomic) UIButton* buttonCloud;
@property (weak, nonatomic) UIButton* buttonInApp;
@property (weak, nonatomic) UIButton* buttonTeamwork;
@property (weak, nonatomic) UIButton* buttonPush;

@property (weak, nonatomic) UIButton* buttonAppstore;
@property (weak, nonatomic) UIButton* buttonDeeplink;
@property (weak, nonatomic) UIButton* buttonSearch;

@property (weak, nonatomic) NSString* listPositions;

@property (strong, nonatomic) NSArray* arrayButtons;

@end

@implementation LCCalculatorController

- (void)loadView {
    [super loadView];
    
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[UIColor whiteColor]];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view endEditing:YES];
    
    [self downloadPrice];
    [self prepareController];
    
}

- (void)downloadPrice {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        self.priceArray = [LCPricesManager downloadPriceFromServer];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    });
    
}

- (void)prepareController {
  
    [self.tableView registerNib:[UINib nibWithNibName:@"LCCalcFirstTextCell" bundle:nil] forCellReuseIdentifier:kIdentifireFirstText];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCDeviceCell" bundle:nil] forCellReuseIdentifier:kIdentifirePlatform];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCPlatformDeviceCell" bundle:nil] forCellReuseIdentifier:kIdentifireDevice];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCAuthCell" bundle:nil] forCellReuseIdentifier:kIdentifireAuth];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCAdditionallyCell" bundle:nil] forCellReuseIdentifier:kIdentifireAdditionally];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCPublicationAndAssistanceCell" bundle:nil] forCellReuseIdentifier:kIdentifirePublicationAndAssistance];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCFinalButton" bundle:nil] forCellReuseIdentifier:kIdentifireFinalButton];
    
    
    self.tableView.backgroundColor = [UIColor applicationMainBackgroundColor];
    
    self.navigationItem.title = @"Калькулятор";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == LCSectionFirstText) {
        
        LCCalcFirstTextCell* cell = (LCCalcFirstTextCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireFirstText];
        
        if (!cell) {
            cell = [[LCCalcFirstTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireFirstText];
        }
        
        cell.contentView.backgroundColor = [UIColor applicationMainBackgroundColor];
        
        return cell;
        
    }
    
    if (indexPath.section == LCSectionPlatform) {
        
        LCDeviceCell* cell = (LCDeviceCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifirePlatform];
        
        if (!cell) {
            cell = [[LCDeviceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifirePlatform];
        }
        
        cell.backgroundColor = [UIColor applicationCalcColorCell];
        cell.iosLabel.text = customIosLabel;
        cell.androidLabel.text = customAndroidLabel;
        self.buttoniOS = cell.iOSButton;
        self.buttonAndroid = cell.androidButton;
        
        return cell;
        
    }
    
    if (indexPath.section == LCSectionDevice) {
        
        LCPlatformDeviceCell* cell = (LCPlatformDeviceCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireDevice];
        
        if (!cell) {
            cell = [[LCPlatformDeviceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireDevice];
        }
        
        cell.backgroundColor = [UIColor applicationCalcColorCell];
        cell.phoneLabel.text = customPhoneLabel;
        cell.tabletLabel.text = customTabletLabel;
        cell.watchLabel.text = customWatchLabel;
        
        self.buttonDevicePhone = cell.buttonPhone;
        self.buttonDeviceTablet = cell.buttonTablet;
        self.buttonDeviceWatch = cell.buttonWatch;
        
        return cell;
    }
    
    if (indexPath.section == LCSectionAuthorization) {
        
        LCAuthCell* cell = (LCAuthCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireAuth];
        
        if (!cell) {
            cell = [[LCAuthCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireAuth];
        }
        
        cell.backgroundColor = [UIColor applicationCalcColorCell];
    
        cell.cabinetLabel.text = customCabinetLabel;
        cell.socialLabel.text = customSocialLabel;
        
        self.buttonCabinet = cell.buttonCabinet;
        self.buttonSocial = cell.buttonSocial;
        
        return cell;
    }
    
    if (indexPath.section == LCSectionAdditionally) {
        
        LCAdditionallyCell* cell = (LCAdditionallyCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireAdditionally];
        
        if (!cell) {
            cell = [[LCAdditionallyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireAdditionally];
        }
        
        cell.backgroundColor = [UIColor applicationCalcColorCell];
        
        cell.inAppLabel.text = customInAppLabel;
        cell.teamworkLabel.text = customTeamworkLabel;
        cell.pushLabel.text = customPushLabel;
        cell.cloudLabel.text = customCloudLabel;
        
        self.buttonCloud = cell.buttonCloud;
        self.buttonInApp = cell.buttonInApp;
        self.buttonTeamwork = cell.buttonTeamWork;
        self.buttonPush = cell.buttonPush;
        
        return cell;
    }
    
    if (indexPath.section == LCSectionPublicationAndAssistance) {
        
        LCPublicationAndAssistanceCell* cell = (LCPublicationAndAssistanceCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifirePublicationAndAssistance];
        
        if (!cell) {
            cell = [[LCPublicationAndAssistanceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifirePublicationAndAssistance];
        }
        
        cell.backgroundColor = [UIColor applicationCalcColorCell];
        
        cell.searchLabel.text = customSearchLabel;
        cell.appstoreLabel.text = customAppstoreLabel;
        cell.deeplinkLabel.text = customDeeplinkLabel;
        
        self.buttonAppstore = cell.buttonAppstore;
        self.buttonDeeplink = cell.buttonDeeplink;
        self.buttonSearch = cell.buttonSearch;
        
        return cell;
    }
    
    if (indexPath.section == LCSectionButton) {
        
        LCFinalButton* cell = (LCFinalButton *)[tableView dequeueReusableCellWithIdentifier:kIdentifireFinalButton];
        
        if (!cell) {
            cell = [[LCFinalButton alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireFinalButton];
        }
        
        cell.backgroundColor = [UIColor applicationMainBackgroundColor];
       
        [cell.buttonFinal setTitleColor:[UIColor applicationCallMeLabelColor] forState:UIControlStateNormal];
        cell.buttonFinal.backgroundColor = [UIColor applicationCallMeButtonColor];
        
        cell.buttonFinal.layer.masksToBounds = YES;
        cell.buttonFinal.layer.cornerRadius = 5.f;
        
        cell.delegate = self;

        
        return cell;
    }
    
    else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"default"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"default"];
        }
        
        cell.backgroundColor = [UIColor applicationCalcColorCell];
        
        return cell;
    }
    
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case LCSectionFirstText:
            return 58.f;
            
        case LCSectionAdditionally:
            return 95.f;
    }
    
    return 90.f;//84.f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    switch (section) {
        case LCSectionFirstText:
            return 5.f;
            
        case LCSectionButton:
            return CGFLOAT_MIN;
            
    }
    
    return 20.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    switch (section) {
        case LCSectionFirstText:
            return 5.f;
            
    }
    
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15.f, 0.5f, tableView.frame.size.width, 20)];
    [label setFont:[UIFont systemFontOfSize:13]];
    [label setTextColor:[UIColor whiteColor]];
    
    switch (section) {
        case LCSectionFirstText:
            label.text = @"";
            break;
            
        case LCSectionPlatform:
            label.text = sectionPlatform;
            break;
            
        case LCSectionDevice:
            label.text = sectionDevice;
            break;
            
        case LCSectionAuthorization:
            label.text = sectionAuthorization;
            break;
            
        case LCSectionAdditionally:
            label.text = sectionAdditionally;
            break;
            
        case LCSectionPublicationAndAssistance:
            label.text = sectionPublicationAndAssistance;
            break;
            
        case LCSectionButton:
            label.text = @"";
            break;
            
    }

    [view addSubview:label];
    [view setBackgroundColor:[UIColor applicationMainBackgroundColor]];
    
    return view;
}

#pragma mark - Actions

- (IBAction)finalAction:(UIButton *)sender {
    
    self.arrayButtons = [self flashBackArrayButton];
    
    NSString* priceiOs = @"0";
    NSString* priceAndroid = @"0";
    NSString* pricePhone = @"0";
    NSString* priceTablet = @"0";
    NSString* priceWatch = @"0";
    NSString* priceCabinet = @"0";
    NSString* priceSocial = @"0";
    NSString* priceCloud = @"0";
    NSString* priceInApp = @"0";
    NSString* priceTeamwork = @"0";
    NSString* pricePush = @"0";
    NSString* priceAppstore = @"0";
    NSString* priceDeeplink = @"0";
    NSString* priceSearch = @"0";
    
    for (LCPricesManager* manager in self.priceArray) {
        priceiOs = manager.platformIOS;
        priceAndroid = manager.platformAndroid;
        pricePhone = manager.devicePhone;
        priceTablet = manager.deviceTablet;
        priceWatch = manager.deviceWatch;
        priceCabinet = manager.authCabinet;
        priceSocial = manager.authSocial;
        priceCloud = manager.addictCloud;
        priceInApp = manager.addictInApp;
        priceTeamwork = manager.addictTeamWork;
        pricePush = manager.addictPush;
        priceAppstore = manager.publAndSettingAppstore;
        priceDeeplink = manager.publAndSettingDeeplink;
        priceSearch = manager.publAndSettingSearch;
        
        [self checkStatus:self.buttoniOS insertPrice:priceiOs];
        [self checkStatus:self.buttonAndroid insertPrice:priceAndroid];
        
        [self checkStatus:self.buttonDevicePhone insertPrice:pricePhone];
        [self checkStatus:self.buttonDeviceTablet insertPrice:priceTablet];
        [self checkStatus:self.buttonDeviceWatch insertPrice:priceWatch];
        
        [self checkStatus:self.buttonCabinet insertPrice:priceCabinet];
        [self checkStatus:self.buttonSocial insertPrice:priceSocial];
        
        [self checkStatus:self.buttonCloud insertPrice:priceCloud];
        [self checkStatus:self.buttonInApp insertPrice:priceInApp];
        [self checkStatus:self.buttonTeamwork insertPrice:priceTeamwork];
        [self checkStatus:self.buttonPush insertPrice:pricePush];
        
        [self checkStatus:self.buttonAppstore insertPrice:priceAppstore];
        [self checkStatus:self.buttonDeeplink insertPrice:priceDeeplink];
        [self checkStatus:self.buttonSearch insertPrice:priceSearch];
        
    }
    
    NSInteger result = [_buttoniOS.priceVolume integerValue] + [self.buttonAndroid.priceVolume integerValue] +
    [self.buttonDevicePhone.priceVolume integerValue] + [self.buttonDeviceTablet.priceVolume integerValue] +
    [self.buttonDeviceWatch.priceVolume integerValue] + [self.buttonCabinet.priceVolume integerValue] +
    [self.buttonSocial.priceVolume integerValue] + [self.buttonCloud.priceVolume integerValue] +
    [self.buttonInApp.priceVolume integerValue] + [self.buttonTeamwork.priceVolume integerValue] +
    [self.buttonPush.priceVolume integerValue] + [self.buttonAppstore.priceVolume integerValue] +
    [self.buttonDeeplink.priceVolume integerValue] + [self.buttonSearch.priceVolume integerValue];
    
    if ((self.priceArray == nil)) {
        [[LCAlertService instance] showAlertButtonOkWithTitle:kWarning  andMessage:kLostConnection inVC:self];
        
    } else {
      
        if (result == 0) {
            
            [[LCAlertService instance] showAlertButtonOkWithTitle:kError andMessage:kNotSelectedOptions inVC:self];
            
        } else {
            
            NSInteger platform = [self.buttoniOS.priceVolume integerValue] + [self.buttonAndroid.priceVolume integerValue];
            NSInteger device = [self.buttonDeviceTablet.priceVolume integerValue] + [self.buttonDevicePhone.priceVolume integerValue] + [self.buttonDeviceWatch.priceVolume integerValue];
            
            if (platform == 0) {
                
                [[LCAlertService instance] showAlertButtonOkWithTitle:kError andMessage:kNotSelectedPlatform inVC:self];
            }
            
            if (device == 0) {
                
                [[LCAlertService instance] showAlertButtonOkWithTitle:kError andMessage:kNotSelectedDevice inVC:self];
            }
            
            if (!(device == 0) && !(platform == 0) ) {
                [self fillPositions];
                
                LCCalculatorPush* vc = [self.storyboard instantiateViewControllerWithIdentifier:kCalculatorPush];
                
                vc.controlPrice = [NSString stringWithFormat:@"%ld", (long)result];
                vc.position = self.listPositions;
                
                self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
                
                [self.view endEditing:YES];
                
                [self.navigationController pushViewController:vc animated:YES];
                
                for (UIButton* button in self.arrayButtons) {
                    [self clearValueButton:button];
                }
                
                
            }
        
        }
    }
}

- (void)checkStatus:(UIButton *)button insertPrice:(NSString *)price {
    
    if (button.selected) {
        
        button.priceVolume = price;
    } else {
        
        button.priceVolume = @"0";
    }
}

- (void)fillPositions {
    
    self.listPositions = [NSString stringWithFormat:@"iOs-%u, Android-%u | Phone-%u, Tablet-%u, Watch-%u | Cabinet-%u, Social-%u | Cloud-%u, InApp-%u, TeamWork-%u, Push-%u | Appstore-%u, Deeplink-%u, Search-%u", self.buttoniOS.selected, self.buttonAndroid.selected, self.buttonDevicePhone.selected, self.buttonDeviceTablet.selected, self.buttonDeviceWatch.selected, self.buttonCabinet.selected, self.buttonSocial.selected, self.buttonCloud.selected, self.buttonInApp.selected, self.buttonTeamwork.selected, self.buttonPush.selected, self.buttonAppstore.selected, self.buttonDeeplink.selected, self.buttonSearch.selected];
   
    //@"A-A%u, A-B%u | B-A%u, B-B%u, B-C%u| C-A%u, C-B%u | D-A%u, D-B%u, D-C%u, D-D%u | E-A%u, E-B%u, E-C%u"
    //A-A1, A-B0 | B-A0, B-B1, B-C1| C-A0, C-B1 | D-A0, D-B0, D-C1, D-D1 | E-A1, E-B0, E-C0
    
}

- (NSArray *)flashBackArrayButton {
    
    NSArray* tempArray = [NSArray arrayWithObjects:self.buttoniOS,
                          self.buttonAndroid,
                          self.buttonDevicePhone,
                          self.buttonDeviceTablet,
                          self.buttonDeviceWatch,
                          self.buttonCabinet,
                          self.buttonSocial,
                          self.buttonCloud,
                          self.buttonInApp,
                          self.buttonTeamwork,
                          self.buttonPush,
                          self.buttonAppstore,
                          self.buttonDeeplink,
                          self.buttonSearch, nil];
    
    
    return tempArray;
}

- (void)clearValueButton:(UIButton *)sender {
    
    sender.selected = NO;
}
@end
