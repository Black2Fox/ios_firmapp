//
//  LCPublicationAndAssistanceCell.m
//  Vizitka_2
//
//  Created by Vio on 25.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCPublicationAndAssistanceCell.h"

@implementation LCPublicationAndAssistanceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIImage* normalOvel = [UIImage imageNamed:@"oval_back"];
    UIImage* selectedOval = [UIImage imageNamed:@"oval_high"];
    
    [self.buttonAppstore setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonAppstore setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonSearch setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonSearch setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonDeeplink setImage:normalOvel forState:UIControlStateNormal];
    [self.buttonDeeplink setImage:selectedOval forState:UIControlStateSelected];
    
    [self.buttonAppstore addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonSearch addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonDeeplink addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
 
}

- (void)buttonTouch:(UIButton *)aButton withEvent:(UIEvent *)event {
    aButton.selected = !aButton.selected;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
