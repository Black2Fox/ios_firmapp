//
//  LCContactsController.m
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCContactsController.h"
#import "LCHelpContacts.h"

#import "LCCreditsController.h"
#import "LCAttachmentController.h"

#import "LCUserDataManager.h"
#import "Backendless.h"
#import "LCValidationService.h"
#import "LCAlertService.h"

#import "DBAttachment.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

static NSString* const kSkypeCompany = @"skype:echo123?call";
static NSString* const kViberCall = @"viber://tel:79032091905";
static NSString* const kShareApp = @"http://insearchapp.ru";

@interface LCContactsController () <SegmentDelegate, MFMailComposeViewControllerDelegate, NameCompanyDelegate, AttachmentDelegate, CollectionAllDataDelegate, UITextViewDelegate>

//Company
@property (strong, nonatomic) NSString* phoneNumberLabel;
@property (strong, nonatomic) NSString* emailLabel;
@property (strong, nonatomic) NSString* skypeLabel;

//Segment
@property (assign, nonatomic) UISegmentedControl* sgTime;
@property (strong, nonatomic) NSString* timeSegment;

//Phone
@property (strong, nonatomic) UITextField* pcPhoneNumber;
@property (strong, nonatomic) UITextField* pcNameUser;

//Attach
@property (strong, nonatomic) UITextView* atText;
@property (strong, nonatomic) UIButton* clipButton;

//ValuePhone
@property (assign, nonatomic) BOOL isEnablePhone;
@property (assign, nonatomic) BOOL isEnableViber;

//test
@property (strong, nonatomic) NSMutableDictionary* textViews;

@end

@implementation LCContactsController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    self.textViews = [NSMutableDictionary new];
    
    [self createButtons];
    [self prepareController];
    [self prepareCustomCell];
    [self createGesture];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if (self.attachmentArray.count >= 1) {
        
        [self.clipButton setImage:[UIImage imageNamed:@"AttachFileBlue"] forState:UIControlStateNormal];
    } else {
        
        [self.clipButton setImage:[UIImage imageNamed:@"AttachFileIcon"] forState:UIControlStateNormal];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)uploadAsync:(NSData *)data andNameFile:(NSString *)name andUserPhone:(NSString *)userPhone {
        
    [backendless.fileService upload:[NSString stringWithFormat:@"UserData/%@/%@.jpeg",userPhone ,name] content:data
                           response:^(BackendlessFile *uploadedFile) {
                              
                           }
                              error:^(Fault *fault) {
                              }];
}

- (void)uploadAsyncFiles:(NSData *)data andNameFile:(NSString *)name andUserPhone:(NSString *)userPhone {
    
    [backendless.fileService upload:[NSString stringWithFormat:@"UserData/%@/%@", userPhone, name] content:data
                           response:^(BackendlessFile *uploadedFile) {
                               
                           }
                              error:^(Fault *fault) {
                              }];
}
- (void)createButtons {
    
    UIBarButtonItem* leftItem = [[UIBarButtonItem alloc] initWithTitle:kDefaultTitleLeft
                                                                 style:UIBarButtonItemStylePlain target:self action:@selector(actionPushCreditsController:)];
    
    UIBarButtonItem* rightItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                               target:self action:@selector(shareButtonClicked)];
    
    
    [self.navigationItem setLeftBarButtonItem:leftItem];
    [self.navigationItem setRightBarButtonItem:rightItem];
    
}

- (void)shareButtonClicked {
 
    NSArray* dataToShare = @[kShareApp];
    UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypeMessage];
    [self presentViewController:activityViewController animated:YES completion:^{}];
}

- (void)createGesture {
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(hideKeyBoard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
}

- (void)prepareController {
    
    self.imageArray = [NSMutableArray new];
    
    self.attachmentArray = [NSMutableArray new];
    
    self.isEnablePhone = YES;
    
    self.navigationController.navigationBar.tintColor = [UIColor applicationBarColor];
    [self.tableView setBackgroundColor:[UIColor applicationMainBackgroundColor]];
}

#pragma mark - Actions

- (void)actionPushCreditsController:(UIBarButtonItem *)sender {
    
    LCCreditsController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LCCreditsController"];
  
    [self.navigationController pushViewController:vc animated:YES];
    [self.view endEditing:YES];
}

#pragma mark - InitCustomCell

- (void)prepareCustomCell {
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LCSegmentCell" bundle:nil] forCellReuseIdentifier:kIdentifireSegmentCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCNameCompanyCell" bundle:nil] forCellReuseIdentifier:kIdentifireNameCompanyCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCPhoneCell" bundle:nil] forCellReuseIdentifier:
        kIdentifirePhoneCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCAttachmentCell" bundle:nil] forCellReuseIdentifier:kIdentifireAttachmentCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCSocialImageCell" bundle:nil] forCellReuseIdentifier:kIdentifireSocialCell];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0){
        
        LCNameCompanyCell* cell = (LCNameCompanyCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireNameCompanyCell];
        if (!cell) {
            cell = [[LCNameCompanyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireNameCompanyCell];
        }
        
        self.phoneNumberLabel = cell.underPhoneLabel.text;
        self.emailLabel = cell.underEmailLabel.text;
        self.skypeLabel = cell.underSkypeLabel.text;
        
        cell.delegate = self;
        
        return cell;
        
    }
    
    if (indexPath.section == 1) {
        
        LCPhoneCell* cell = (LCPhoneCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifirePhoneCell];
        if (!cell) {
            cell = [[LCPhoneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifirePhoneCell];
        }
        
        self.pcPhoneNumber = cell.phoneField;
        self.pcNameUser = cell.nameField;
        
        return cell;

    }
    
    if (indexPath.section == 2) {
        
        LCSegmentCell* cell = (LCSegmentCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireSegmentCell];
        if (!cell) {
            cell = [[LCSegmentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireSegmentCell];
        }
        
        self.sgTime = cell.timeSegment;
        cell.delegate = self;
        
        return cell;
    }
    
    if (indexPath.section == 3) {
        
        LCAttachmentCell* cell = (LCAttachmentCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireAttachmentCell];
        if (!cell) {
            cell = [[LCAttachmentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireAttachmentCell];
        }
        
        self.atText = cell.attachTextView;
        cell.delegate = self;
        cell.delegateCollection = self;
        
        self.clipButton = cell.attachButton;
        cell.attachButton = self.clipButton;
        
        [self.textViews setObject:cell.attachTextView forKey:indexPath];
        [cell.attachTextView setDelegate:self];
    
        return cell;
    }
    
    if (indexPath.section == 4) {
        
        LCSocialImageCell* cell = (LCSocialImageCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireSocialCell];
        if (!cell) {
            cell = [[LCSocialImageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireSocialCell];
        }
        
        return cell;
    }
    else {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reserved"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reserved"];
        }
        
        return cell;
    }
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case 0:
            return 217.f;
            
        case 1:
            return 100.f;
            
        case 2:
            return 55.f;
            
        case 3:
            return [self textViewHeightForRowAtIndexPath:indexPath] + 80;//130.f;
            
        case 4:
            return 50.f;
            
    }
    
    return 44.f;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == nameCompanyCell){
        
        [LCHelpContacts settingNameCompanyCell:(LCNameCompanyCell *)cell];
    }
    
    if (indexPath.section == phoneCell) {
        
        [LCHelpContacts settingPhoneCell:(LCPhoneCell *)cell];
    }
    
    if (indexPath.section == segmentCell) {
        
        [LCHelpContacts settingSegmentCell:(LCSegmentCell *)cell];
    }
    
    if (indexPath.section == attachmentCell) {
        
       [LCHelpContacts settingAttachmentCell:(LCAttachmentCell *)cell];
    }
        
    if (indexPath.section == socialImageCell) {
        
        [LCHelpContacts settingSocialImageCell:(LCSocialImageCell *)cell];
    }
    
}

- (NSString *)itsTime {
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"_yyyy-MM-dd_HH:mm:ss"];
    NSString* timeNow = [dateFormatter stringFromDate:[NSDate date]];
    
    NSLog(@"%@", timeNow);
    return timeNow;
    
}

#pragma mark - Actions

- (IBAction)actionCollectData:(id)sender {
    
    self.timeSegment = [self convertTime:self.sgTime];
    
    NSString* tempUserPhone = [@"+7 " stringByAppendingString:self.pcPhoneNumber.text];
    NSString* tempUser = [tempUserPhone stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString* tempUserWithTime = [tempUser stringByAppendingString:[self itsTime]];
    
    BOOL checkPhone;
    BOOL checkName;
    
    if (tempUserPhone.length == 18) {
        checkPhone = YES;
    } else {
        checkPhone = NO;
    }
    
    if (self.pcNameUser.text.length >= 2) {
        checkName = YES;
    } else {
        checkName = NO;
    }
    
    if (checkPhone == YES && checkName == YES) {
        
        [backendless.persistenceService save:[LCUserDataManager sendToServerUserInfoContactPhone:tempUserPhone andContactName:self.pcNameUser.text andTime:self.timeSegment andAttachment:self.atText.text]];
        
        [[LCAlertService instance] showAlertButtonOkWithTitle:kSuccessSendTicket andMessage:[NSString stringWithFormat:@"%@\nВремя звонка %@",kWaitCallCompany , self.timeSegment] inVC:self];


        if ([self.attachmentArray count] >= 1) {
            
            NSInteger countFiles = 0;
            for (DBAttachment* attach in self.attachmentArray) {
              
                if (attach.mediaType == DBAttachmentMediaTypeImage) {
                    NSInteger i = 0;
                    for (UIImage* image in self.imageArray) {
                        NSData* data = [NSData new];
                        data = UIImageJPEGRepresentation(image, 0.8);
                        NSString* tempString = [NSString stringWithFormat:@"Attach-%ld", (long)i];
                        [self uploadAsync:data andNameFile:tempString andUserPhone:tempUserWithTime];
                        i++;
                    }
                }
                
                else {
                    
                    NSString* urlString = [attach originalFileResource];
                    
                    NSString* fileFormat = attach.fileName;
                    
                    NSArray *arr = [fileFormat componentsSeparatedByString:@"."];
                    NSString *strSubStringDigNum = [arr lastObject];
                    
                    NSString* tempString = [NSString stringWithFormat:@"Files-%ld.%@", (long)countFiles,strSubStringDigNum];
                    
                    NSURL* url = [NSURL fileURLWithPath:urlString isDirectory:YES];
                    NSData* data = [NSData dataWithContentsOfURL:url];
                    
                    [self uploadAsyncFiles:data andNameFile:tempString andUserPhone:tempUserWithTime];
                    countFiles++;
                }
      
            }
        }
        [self clearAllFields];
        
        [self.clipButton setImage:[UIImage imageNamed:@"AttachFileIcon"] forState:UIControlStateNormal];
        
        [self.view endEditing:YES];
    }
    
    if (checkPhone == NO) {
        [[LCAlertService instance] showAlertButtonOkWithTitle:kError andMessage:kFalseFilledPhone inVC:self];
    }
    
    if (checkName == NO) {
        [[LCAlertService instance] showAlertButtonOkWithTitle:kError andMessage:kFalseFilledName inVC:self];
    }
    
}

-(void)hideKeyBoard {
    [self.view endEditing:YES];
    
}

- (void)clearAllFields {
 
    self.pcPhoneNumber.text = @"";
    self.atText.text = @"";
    self.imageArray = [NSMutableArray new];
    self.attachmentArray = [NSMutableArray new];
    self.pcNameUser.text = @"";
    
}

- (NSString *)convertTime:(UISegmentedControl *)segment {
    
    NSString* tempString = @"";
    
    switch (segment.selectedSegmentIndex) {
        case 0:
            return tempString = @"8:00-13:00";
            
        case 1:
            return tempString = @"13:00-19:00";
            
        case 2:
            return tempString = @"19:00-0:00";
            
        case 3:
            return tempString = @"0:00-8:00";

    }
    
    return tempString;
}

- (IBAction)actionSegment:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)emailAction:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"Тема письма"];
        [mail setMessageBody:@"Текст сообщения" isHTML:NO];
        [mail setToRecipients:@[self.emailLabel]];
        
        [self presentViewController:mail animated:YES completion:NULL];
        
    } else {
        [[LCAlertService instance] showAlertButtonOkWithTitle:kError andMessage:kMissedMailAgent inVC:self];
    }
    
}

- (IBAction)skypeAction:(id)sender {
    
    BOOL installed = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"skype:"]];
    
    if (installed) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kSkypeCompany]];
        
    } else {
        
        [[LCAlertService instance] showAlertSkypeInVC:self];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)callAction:(id)sender {

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt:+79009009090"]];
}

- (IBAction)actionPhone:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt:+79009009090"]];
}

- (IBAction)actionPushAttachment:(id)sender {
    
    LCAttachmentController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LCAttachmentController"];
    
    vc.attachmentArray = self.attachmentArray;
    vc.imagearray = self.imageArray;
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:vc animated:NO];
    
    [self.view endEditing:YES];
    
}

- (CGFloat)textViewHeightForRowAtIndexPath: (NSIndexPath*)indexPath {
    UITextView *calculationView = [self.textViews objectForKey: indexPath];
    CGFloat textViewWidth = calculationView.frame.size.width;
    if (!calculationView.attributedText) {
        
        calculationView = [[UITextView alloc] init];
       
        textViewWidth = 290.0;
    }
    CGSize size = [calculationView sizeThatFits:CGSizeMake(textViewWidth, FLT_MAX)];
    return size.height;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [self scrollToCursorForTextView:textView];
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
   
    
    if ([textView.text isEqualToString:@"Например, iOS-версия сайта"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
    [self scrollToCursorForTextView:textView];
}

- (void)scrollToCursorForTextView: (UITextView*)textView {
    
    CGRect cursorRect = [textView caretRectForPosition:textView.selectedTextRange.start];
    
    cursorRect = [self.tableView convertRect:cursorRect fromView:textView];
    
    if (![self rectVisible:cursorRect]) {
        cursorRect.size.height += 8;
        [self.tableView scrollRectToVisible:cursorRect animated:YES];
    }
}

- (BOOL)rectVisible: (CGRect)rect {
    CGRect visibleRect;
    visibleRect.origin = self.tableView.contentOffset;
    visibleRect.origin.y += self.tableView.contentInset.top;
    visibleRect.size = self.tableView.bounds.size;
    visibleRect.size.height -= self.tableView.contentInset.top + self.tableView.contentInset.bottom;
    
    return CGRectContainsRect(visibleRect, rect);
}


- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Например, iOS-версия сайта";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}
@end


