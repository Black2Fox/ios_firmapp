//
//  LCLowGroundContent.h
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCLowGroundContent : UITableViewCell <CAAnimationDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollMainView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (assign, nonatomic) CGFloat sizeWidhtWindow;
@property (assign, nonatomic) CGFloat sizeHightWindow;


@property (assign, nonatomic) BOOL internetStatus;


- (IBAction)actionPageControl:(id)sender;

@end

