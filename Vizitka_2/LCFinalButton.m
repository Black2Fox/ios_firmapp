//
//  LCFinalButton.m
//  Vizitka_2
//
//  Created by Vio on 25.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCFinalButton.h"

@implementation LCFinalButton

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)finalAction:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(finalAction:)]) {
        
        [self.delegate finalAction:sender];
    }
    
}

@end
