//
//  LCSegmentCell.m
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCSegmentCell.h"

@interface LCSegmentCell ()

@property (assign, nonatomic) BOOL isAppear;
@property (assign, nonatomic) CGFloat widthWindow;

@end

@implementation LCSegmentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.isAppear = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.widthWindow = self.contentView.frame.size.width;
    
    if (self.isAppear) {
        [self prepareCell];
    }
}

- (void) prepareCell {
    
    CGFloat percentSize;
    if (self.widthWindow == 320) {
        percentSize = 1.f;
    } else if (self.widthWindow == 375) {
        percentSize = 1.17f;
    } else if (self.widthWindow == 414) {
        percentSize = 1.3f;
    }
    
    [self.timeLabel setFont:[UIFont systemFontOfSize:11 * percentSize]];
    
    self.timeSegment.selectedSegmentIndex = 1;
    
    self.isAppear = NO;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (IBAction)actionSegment:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(actionSegment:)]) {
        
        [self.delegate actionSegment:sender];
    }
    
}
@end
