//
//  LCAAPhotoCell.m
//  Vizitka_2
//
//  Created by Vio on 01.12.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCAAPhotoCell.h"

@implementation LCAAPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)downloadAction:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(downloadAction:)]) {
        
        [self.delegate downloadAction:sender];
    }
    
}

@end
