//
//  LCCalcConst.h
//  Vizitka_2
//
//  Created by Vio on 22.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

//имена разделов
static NSString* const sectionDevice = @"Устройство*";
static NSString* const sectionPlatform = @"Платформа*";
static NSString* const sectionAuthorization = @"Авторизация";
static NSString* const sectionAdditionally  = @"Дополнительно";
static NSString* const sectionPublicationAndAssistance = @"Публикация и помощь в настройке";

//лейблы над кнопками
static NSString* const customIosLabel = @"iOS";
static NSString* const customAndroidLabel = @"Android";

static NSString* const customPhoneLabel = @"Смартфоны";
static NSString* const customTabletLabel = @"Планшеты";
static NSString* const customWatchLabel = @"Часы";

static NSString* const customCabinetLabel = @"Личный кабинет";
static NSString* const customSocialLabel = @"Через соцсети";

static NSString* const customCloudLabel = @"Облако данных";
static NSString* const customInAppLabel = @"Внутренние покупки";
static NSString* const customTeamworkLabel = @"Пользовательское взаимодействие";
static NSString* const customPushLabel = @"Push-уведомления";

static NSString* const customAppstoreLabel = @"Публикация";
static NSString* const customDeeplinkLabel = @"Диплинкинг";
static NSString* const customSearchLabel = @"Ключевые слова";

//ячейки
static NSString* const kIdentifireFirstText = @"firstTextCell";
static NSString* const kIdentifirePlatform = @"devPlatformCell";
static NSString* const kIdentifireDevice = @"platformDeviceCell";
static NSString* const kIdentifireAuth = @"authCell";
static NSString* const kIdentifireAdditionally = @"additionallyCell";
static NSString* const kIdentifirePublicationAndAssistance = @"publicationAndAssistanceCell";
static NSString* const kIdentifireFinalButton = @"finalButtonCell";


//Пуш
static NSString* const kCalculatorPush = @"LCCalculatorPush";

/** tag's button
 
 button IOS = 1
 button Android = 2
 
 button Phone = 11
 button Tablet = 12
 button Watch = 13
 
 button Cabinet = 21
 button Social = 22
 
 button Cloud = 31
 button InApp = 32
 button TeamWork = 33
 button Push = 34
 
 button Appstore = 41
 button Deeplink = 42
 button Search = 43
 
 */

@interface LCCalcConst : NSObject

@end
