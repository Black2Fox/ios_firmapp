//
//  LCLowGHelper.h
//  Vizitka_2
//
//  Created by Vio on 17.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CustomLowgroundDelegate;

@interface LCLowGHelper : NSObject

+ (UILabel *)settingFirstLabel:(CGRect)rect andText:(NSString *)text andPercent:(CGFloat)percent;

+ (UILabel *)settingSecondLabel:(CGRect)rect andText:(NSString *)text andPercent:(CGFloat)percent;

- (UIButton *)settingButton:(CGRect)rect andText:(NSString *)text andPercent:(CGFloat)percent andURL:(NSURL *)url;


@property (nonatomic) id<CustomLowgroundDelegate> delegate;

@end


@protocol CustomLowgroundDelegate <NSObject>

@optional

- (void)tabButtonWithParameter:(UIButton *)sender;

@end
