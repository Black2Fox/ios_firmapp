//
//  LCAttachmentController.h
//  Vizitka_2
//
//  Created by Vio on 05.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCAttachmentController : UITableViewController

@property (strong, nonatomic) NSMutableArray *attachmentArray;
@property (strong, nonatomic) NSMutableArray *imagearray;

@end

