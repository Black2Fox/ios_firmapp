//
//  LCWorkSpaceCell.m
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCWorkSpaceCell.h"
#import "LCWorkSpaceConstantClass.h"

@interface LCWorkSpaceCell ()

@property (assign, nonatomic) BOOL isAppear;

@end

@implementation LCWorkSpaceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.isAppear = YES;
    [self.contentView addSubview:self.segmentBarView];
    [self.contentView addSubview:self.segmentControl];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    [super setHighlighted:NO animated:animated];

    // Configure the view for the selected state
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.sizeHightWindow = self.mainWorkStudio.frame.size.height;
    self.sizeWidhtWindow = self.mainWorkStudio.frame.size.width;
    
    if (self.isAppear) {
        [self prepareCell];
    }
}

- (void)prepareCell {
   
    self.segmentControl.tintColor = [UIColor applicationSegmentsNopeHighlights];
    
    UIFont *font = [UIFont systemFontOfSize:13.f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    [self.segmentControl setTitleTextAttributes:attributes
                                    forState:UIControlStateNormal];
    
    //ширина экрана
    CGFloat widthWindow = self.sizeWidhtWindow;
    
    //узнаем увеличение
    
    CGFloat percentSize;
    if (widthWindow == 320) {
        percentSize = 1.f;
    } else if (widthWindow == 375) {
        percentSize = 1.17f;
    } else if (widthWindow == 414) {
        percentSize = 1.3f;
    }
    
    //размеры вью на скроле
    CGFloat height = self.sizeHightWindow - 44;
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, widthWindow * 2, height)];
    
    view.backgroundColor = [UIColor clearColor];
    
    //оптимальная зависимость
    CGFloat optimalProportion = 1.5f;
    
    //первый экран
    CGFloat halfSizeView = view.frame.size.width / 2;
    CGFloat heightContent = view.frame.size.height;
    
    //размер и положение первой картинки
    CGFloat xImage1 = widthWindow / optimalProportion;
    CGFloat yImage1 = widthWindow / optimalProportion;
    
    CGFloat imageViewStartPositionY = heightContent / 2 - xImage1 / 2;
    CGFloat imageViewStartPositionX = halfSizeView / 2 - xImage1 / 2;
    
    UIImageView* imageRect = [[UIImageView alloc] initWithFrame:CGRectMake(imageViewStartPositionX, imageViewStartPositionY, xImage1, yImage1)];
    
    UIImage *image = [UIImage imageNamed:@"icon"];//icon_app
    imageRect.image = image;
    
    
    //второй экран
    
    //размер и положение первой картинки
    
    CGFloat optimalSecoundProportion = 6.4f;
    CGFloat secondWindowImageX = widthWindow / optimalSecoundProportion;
    CGFloat secondWindowImageY = widthWindow / optimalSecoundProportion;
    
    CGFloat firstImageStartY = imageViewStartPositionY + secondWindowImageY - 7;//контроль высоты картинок второго экрана первого блока
    CGFloat firstImageStartX = imageViewStartPositionX + halfSizeView + secondWindowImageX / 8;
    
    UIImageView* firstImageSecondWindows = [[UIImageView alloc] initWithFrame:CGRectMake(firstImageStartX, firstImageStartY, secondWindowImageX, secondWindowImageY)];
    
    UIImage *firstSecond = [UIImage imageNamed:@"terms"];
    firstImageSecondWindows.image = firstSecond;
    
    //размер и положение второй картинки
    
    CGFloat secondImageStartY = firstImageStartY;
    CGFloat secondImageStartX = firstImageStartX + secondWindowImageX * 3;
    
    UIImageView* secondImageSecondWindows = [[UIImageView alloc] initWithFrame:CGRectMake(secondImageStartX, secondImageStartY, secondWindowImageX + 5, secondWindowImageY)];
    
    UIImage *secondSecond = [UIImage imageNamed:@"feedback"];
    secondImageSecondWindows.image = secondSecond;
    
    //размер и положение третьей картинки
    
    CGFloat thirdImageStartY = firstImageStartY + heightContent / 2.7f;//контроль высоты картинок второго экрана второго блока
    CGFloat thirdImageStartX = firstImageStartX;
    
    UIImageView* thirdImageSecondWindows = [[UIImageView alloc] initWithFrame:CGRectMake(thirdImageStartX, thirdImageStartY, secondWindowImageX, secondWindowImageY)];
    
    UIImage *thirdSecond = [UIImage imageNamed:@"otchet"];
    thirdImageSecondWindows.image = thirdSecond;
    
    //размер и положение четвертой картинки
    
    CGFloat fourImageStartY = thirdImageStartY;
    CGFloat fourImageStartX = secondImageStartX;
    
    UIImageView* fourImageSecondWindows = [[UIImageView alloc] initWithFrame:CGRectMake(fourImageStartX, fourImageStartY, secondWindowImageX + 5, secondWindowImageY)];
    
    UIImage *fourSecond = [UIImage imageNamed:@"safeguard"];
    fourImageSecondWindows.image = fourSecond;
    
    //Главный лейбл
    
    CGFloat mainLabelX = firstImageStartX - secondWindowImageX / 1.8;
    CGFloat mainLabelY = firstImageStartY - firstImageStartY * 0.8f;
    
    CGFloat mainLabelWidht = secondImageStartX / 2;
    CGFloat mainLabelHight = 55 * percentSize;
    
    UILabel* labelMainText = [[UILabel alloc] initWithFrame:CGRectMake(mainLabelX, mainLabelY, mainLabelWidht, mainLabelHight)];
    labelMainText.text = kMainMessageSecondWindow;
    labelMainText.textAlignment = NSTextAlignmentCenter;
    [labelMainText setFont:[UIFont systemFontOfSize:14 * percentSize]];
    labelMainText.numberOfLines = 3;
    
    //Первый лейбл
    CGFloat termsLabelX = firstImageStartX - secondWindowImageX / 1.2f;
    CGFloat termsLabelY = firstImageStartY + secondWindowImageY * percentSize;
    
    CGFloat termsLabelWidht = secondImageStartX / 4;
    CGFloat termsLabelHight = 30 * percentSize;// 40
    
    UILabel* labelTerms = [[UILabel alloc] initWithFrame:CGRectMake(termsLabelX, termsLabelY, termsLabelWidht, termsLabelHight)];
    
    labelTerms.text = kTermsText;
    labelTerms.textAlignment = NSTextAlignmentCenter;
    [labelTerms setFont:[UIFont systemFontOfSize:10.5 * percentSize]];
    labelTerms.numberOfLines = 3;
    
    //второй лейбл
    
    CGFloat feedbackLabelX = termsLabelX + termsLabelWidht * 1.3f;
    CGFloat feedbackLabelY = firstImageStartY + secondWindowImageY * percentSize;
    
    CGFloat feedbackLabelWidht = secondImageStartX / 6;
    CGFloat feedbackLabelHight = 30 * percentSize;
    
    UILabel* labelFeedback = [[UILabel alloc] initWithFrame:CGRectMake(feedbackLabelX, feedbackLabelY, feedbackLabelWidht, feedbackLabelHight)];
    
    labelFeedback.text = kFeedbackText;
    labelFeedback.textAlignment = NSTextAlignmentCenter;
    [labelFeedback setFont:[UIFont systemFontOfSize:11 * percentSize]];
    labelFeedback.numberOfLines = 2;
    
    //третий лейбл
    
    CGFloat reportLabelX = termsLabelX + termsLabelWidht / 6;
    CGFloat reportLabelY = termsLabelY + heightContent /2.7f;
    
    CGFloat reportLabelWidht = secondImageStartX / 6;
    CGFloat reportLabelHight = 30 * percentSize;
    
    UILabel* labelReport = [[UILabel alloc] initWithFrame:CGRectMake(reportLabelX, reportLabelY, reportLabelWidht, reportLabelHight)];
    
    labelReport.text = kReportText;
    labelReport.textAlignment = NSTextAlignmentCenter;
    [labelReport setFont:[UIFont systemFontOfSize:11 * percentSize]];
    labelReport.numberOfLines = 2;
    
    //четвертый лейбл
    
    CGFloat safeguardLabelX = feedbackLabelX;// тут нюанс
    CGFloat safeguardLabelY = feedbackLabelY + heightContent / 2.7f;
    
    CGFloat safeguardLabelWidht = secondImageStartX / 5.5f;
    CGFloat safeguardLabelHight = 30 * percentSize;//40
    
    UILabel* labelSafeguard = [[UILabel alloc] initWithFrame:CGRectMake(safeguardLabelX, safeguardLabelY, safeguardLabelWidht, safeguardLabelHight)];
    
    labelSafeguard.text = kSafeGuardText;
    labelSafeguard.textAlignment = NSTextAlignmentCenter;
    [labelSafeguard setFont:[UIFont systemFontOfSize:11 * percentSize]];
    labelSafeguard.numberOfLines = 3;
    
    //лейблы первого экрана
    
    //первый
    CGFloat labelFirstWindowX = mainLabelX - halfSizeView - secondWindowImageX / 4;
    CGFloat labelFirstWindowY = mainLabelY;
    
    CGFloat labelFirstWindowWidth = xImage1 * 1.3335f;
    CGFloat labelFirstWindowHeight = 35 * percentSize;
    
    UILabel* labelFirstWindowOne = [[UILabel alloc] initWithFrame:CGRectMake(labelFirstWindowX, labelFirstWindowY, labelFirstWindowWidth, labelFirstWindowHeight)];
    labelFirstWindowOne.text = kMainMessageFirstWindow;
    labelFirstWindowOne.textAlignment = NSTextAlignmentLeft;
    [labelFirstWindowOne setFont:[UIFont systemFontOfSize:12.5 * percentSize]];
    labelFirstWindowOne.numberOfLines = 2;
    labelFirstWindowOne.lineBreakMode = NSLineBreakByWordWrapping;
    
    //второй
    CGFloat labelTwoFirstWindowX = labelFirstWindowX;
    CGFloat labelTwoFirstWindowY = labelFirstWindowY * 4;
    
    CGFloat labelTwoFirstWindowWidth = xImage1 * 1.3335f;
    CGFloat labelTwoFirstWindowHeight = 50 * percentSize;
    
    UILabel* labelTwoFirstWindowOne = [[UILabel alloc] initWithFrame:CGRectMake(labelTwoFirstWindowX, labelTwoFirstWindowY, labelTwoFirstWindowWidth, labelTwoFirstWindowHeight)];
    labelTwoFirstWindowOne.text = kTwoMainMessageFirstWindow;
    labelTwoFirstWindowOne.textAlignment = NSTextAlignmentLeft;
    [labelTwoFirstWindowOne setFont:[UIFont systemFontOfSize:12.5 * percentSize]];
    labelTwoFirstWindowOne.numberOfLines = 3;
    labelTwoFirstWindowOne.lineBreakMode = NSLineBreakByWordWrapping;
    
    //третий(1/4)
    CGFloat labelItem1X = labelFirstWindowX;
    CGFloat labelItem1Y = labelFirstWindowY * 8;
    
    CGFloat labelItem1Width = 200;
    CGFloat labelItem1Height = 20;
    
    UILabel* labelItem1 = [[UILabel alloc] initWithFrame:CGRectMake(labelItem1X, labelItem1Y, labelItem1Width, labelItem1Height)];
    labelItem1.text = kItem1;
    [labelItem1 setFont:[UIFont systemFontOfSize:13.5f * percentSize]];
    
    //четвертый(2/4)
    CGFloat labelItem2X = labelFirstWindowX;
    CGFloat labelItem2Y = labelFirstWindowY * 9;
    
    CGFloat labelItem2Width = 200;
    CGFloat labelItem2Height = 20;
    
    UILabel* labelItem2 = [[UILabel alloc] initWithFrame:CGRectMake(labelItem2X, labelItem2Y, labelItem2Width, labelItem2Height)];
    labelItem2.text = kItem2;
    [labelItem2 setFont:[UIFont systemFontOfSize:13.5f * percentSize]];
    
    //пятый(3/4)
    CGFloat labelItem3X = labelFirstWindowX;
    CGFloat labelItem3Y = labelFirstWindowY * 10;
    
    CGFloat labelItem3Width = 300;
    CGFloat labelItem3Height = 20;
    
    UILabel* labelItem3 = [[UILabel alloc] initWithFrame:CGRectMake(labelItem3X, labelItem3Y, labelItem3Width, labelItem3Height)];
    labelItem3.text = kItem3;
    [labelItem3 setFont:[UIFont systemFontOfSize:13.5f * percentSize]];
    
    //шестой(4/4)
    CGFloat labelItem4X = labelFirstWindowX;
    CGFloat labelItem4Y = labelFirstWindowY * 11;
    
    CGFloat labelItem4Width = 200;
    CGFloat labelItem4Height = 20;
    
    UILabel* labelItem4 = [[UILabel alloc] initWithFrame:CGRectMake(labelItem4X, labelItem4Y, labelItem4Width, labelItem4Height)];
    labelItem4.text = kItem4;
    [labelItem4 setFont:[UIFont systemFontOfSize:13.5f * percentSize]];
    
    //седьмой
    CGFloat basementFirstWindowX = labelFirstWindowX;
    CGFloat basementFirstWindowY = labelFirstWindowY * 13.5f;
    
    CGFloat basementFirstWindowWidth = xImage1 * 1.3335f;
    CGFloat basementFirstWindowHeight = 50 * percentSize;
    
    UILabel* basementFirstWindowOne = [[UILabel alloc] initWithFrame:CGRectMake(basementFirstWindowX, basementFirstWindowY, basementFirstWindowWidth, basementFirstWindowHeight)];
    basementFirstWindowOne.text = kBaseMentWindows;
    basementFirstWindowOne.textAlignment = NSTextAlignmentLeft;
    [basementFirstWindowOne setFont:[UIFont systemFontOfSize:12.5 * percentSize]];
    basementFirstWindowOne.numberOfLines = 3;
    basementFirstWindowOne.lineBreakMode = NSLineBreakByWordWrapping;
    
    
    [view addSubview:imageRect];
    [view addSubview:firstImageSecondWindows];
    [view addSubview:secondImageSecondWindows];
    [view addSubview:thirdImageSecondWindows];
    [view addSubview:fourImageSecondWindows];
    [view addSubview:labelMainText];
    [view addSubview:labelTerms];
    [view addSubview:labelFeedback];
    [view addSubview:labelReport];
    [view addSubview:labelSafeguard];
    [view addSubview:labelFirstWindowOne];
    [view addSubview:labelTwoFirstWindowOne];
    [view addSubview:labelItem1];
    [view addSubview:labelItem2];
    [view addSubview:labelItem3];
    [view addSubview:labelItem4];
    [view addSubview:basementFirstWindowOne];
    
    self.contentScrollView.backgroundColor = [UIColor clearColor];
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    self.contentScrollView.pagingEnabled = YES;
    [self.contentScrollView setDelegate:self];
    [self.contentScrollView setShowsVerticalScrollIndicator:NO];
    [self.contentScrollView setShowsHorizontalScrollIndicator:NO];
    [self.contentScrollView addSubview:view];
    
    self.contentScrollView.contentSize = CGSizeMake(view.frame.size.width, self.contentScrollView.frame.size.height);
    
    self.isAppear = NO;
    
}

- (IBAction)actionSegmentControl:(id)sender {
    
    NSInteger page = (int)self.segmentControl.selectedSegmentIndex;
    
    CGRect frame = self.contentScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.contentScrollView scrollRectToVisible:frame animated:YES];
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger page = scrollView.contentOffset.x/scrollView.frame.size.width;
    self.segmentControl.selectedSegmentIndex = page;
    
}

@end
