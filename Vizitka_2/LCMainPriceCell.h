//
//  LCMainPriceCell.h
//  Vizitka_2
//
//  Created by Vio on 14.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCMainPriceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *mainViewContent;
@property (weak, nonatomic) IBOutlet UILabel *firstPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelPriceWithProc;
@property (weak, nonatomic) IBOutlet UILabel *addictUserLabel;

@end
