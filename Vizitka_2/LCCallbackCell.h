//
//  LCCallbackCell.h
//  Vizitka_2
//
//  Created by Vio on 14.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SendMailDelegate;

@interface LCCallbackCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *botLabel;

- (IBAction)actionCall:(id)sender;

@property (nonatomic) id<SendMailDelegate> delegate;

@end

@protocol SendMailDelegate <NSObject>

@optional

- (IBAction)actionEmail:(id)sender;

@end
