//
//  LCPortfolioController.h
//  Vizitka_2
//
//  Created by Vio on 09.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCPortfolioController : UIViewController

@property (strong, nonatomic) NSArray* arrayContent;

@end
