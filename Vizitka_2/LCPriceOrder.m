//
//  LCPriceOrder.m
//  Vizitka_2
//
//  Created by Vio on 28.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCPriceOrder.h"

@implementation LCPriceOrder

+ (LCPriceOrder *)sendToServerInfoOrder:(NSString *)uPosition andResultPrice:(NSString *)uPrice phone:(NSString *)uPhone email:(NSString *)uEmail {
    LCPriceOrder* newOrder = [LCPriceOrder new];
    
    newOrder.position = uPosition;
    newOrder.resultPrice = uPrice;
    newOrder.userPhone = uPhone;
    newOrder.userEmail = uEmail;
    
    return newOrder;
}

- (void)sendUserInfoToServer {
    
//    [backendless.persistenceService save:[LCPriceOrder sendToServerInfoOrder:@"POSITIONS" andResultPrice:@"RESULTPRICE" phone:@"USERPHONE" email:@"USERMAIL"]];
}



@end
