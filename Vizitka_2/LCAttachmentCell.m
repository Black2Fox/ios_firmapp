//
//  LCAttachmentCell.m
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCAttachmentCell.h"

@interface LCAttachmentCell ()

@property (assign, nonatomic) BOOL isAppear;
@property (assign, nonatomic) CGFloat widthWindow;


@end

@implementation LCAttachmentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.isAppear = YES;
    
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    self.attachTextView.inputAccessoryView = keyboardToolbar;
    
    
    self.attachTextView.text = @"Например, iOS-версия сайта";
    self.attachTextView.textColor = [UIColor lightGrayColor];
}

-(void)yourTextViewDoneButtonPressed
{
    [self.attachTextView resignFirstResponder];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.widthWindow = self.contentView.frame.size.width;
    
    if (self.isAppear) {
        [self prepareCell];
    }
}

- (void) prepareCell {
    
    CGFloat percentSize;
    if (self.widthWindow == 320) {
        percentSize = 1.f;
    } else if (self.widthWindow == 375) {
        percentSize = 1.17f;
    } else if (self.widthWindow == 414) {
        percentSize = 1.3f;
    }
    
    [self.attachmentLabel setFont:[UIFont systemFontOfSize:11 * percentSize]];
    
    self.isAppear = NO;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionPushAttachment:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(actionPushAttachment:)]) {
        
        [self.delegate actionPushAttachment:sender];
    }
    
}
- (IBAction)actionCollectData:(id)sender {
    
    if ([self.delegateCollection respondsToSelector:@selector(actionCollectData:)]) {
        
        [self.delegateCollection actionCollectData:sender];
    }
    
}

@end
