//
//  LCContactsController.h
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger) {
    
    nameCompanyCell     = 0,
    phoneCell           = 1,
    segmentCell         = 2,
    attachmentCell      = 3,
    socialImageCell     = 4
    
} numberofCell;

@interface LCContactsController : UITableViewController

@property (strong, nonatomic) NSMutableArray* attachmentArray;
@property (strong, nonatomic) NSMutableArray* imageArray;

@end


