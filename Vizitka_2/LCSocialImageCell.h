//
//  LCSocialImageCell.h
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCSocialImageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *buttonBe;
@property (weak, nonatomic) IBOutlet UIButton *buttonIn;
@property (weak, nonatomic) IBOutlet UIButton *buttonFb;
@property (weak, nonatomic) IBOutlet UIButton *buttonTwitter;
- (IBAction)actionBe:(UIButton *)sender;

- (IBAction)actionIn:(UIButton *)sender;
- (IBAction)actionFb:(UIButton *)sender;
- (IBAction)actionTwitter:(UIButton *)sender;
@end
