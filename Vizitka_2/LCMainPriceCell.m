//
//  LCMainPriceCell.m
//  Vizitka_2
//
//  Created by Vio on 14.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCMainPriceCell.h"

@interface LCMainPriceCell ()

@property (assign, nonatomic) CGFloat widthWindow;
@property (assign, nonatomic) BOOL isAppear;

@end

@implementation LCMainPriceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.isAppear = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.widthWindow = self.contentView.frame.size.width;
    
    if (self.isAppear) {
        [self prepareCell];
    }
}


- (void) prepareCell {
    
    CGFloat percentSize;
    if (self.widthWindow == 320) {
        percentSize = 1.f;
    } else if (self.widthWindow == 375) {
        percentSize = 1.17f;
    } else if (self.widthWindow == 414) {
        percentSize = 1.3f;
    }
    
    [self.firstPriceLabel setFont:[UIFont systemFontOfSize:13 * percentSize]];
    [self.secondPriceLabel setFont:[UIFont systemFontOfSize:11 * percentSize]];
    [self.labelPrice setFont:[UIFont systemFontOfSize:13 * percentSize]];
    [self.labelPriceWithProc setFont:[UIFont systemFontOfSize:11 * percentSize]];
    [self.addictUserLabel setFont:[UIFont systemFontOfSize:13 * percentSize]];
    
    self.isAppear = NO;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
