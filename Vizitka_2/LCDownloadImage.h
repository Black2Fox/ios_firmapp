//
//  LCUploadImage.h
//  Vizitka_2
//
//  Created by Vio on 15.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LCDownloadImage : NSObject

@property (strong, nonatomic) NSString* imageURL;

@property (nonatomic, strong) NSString* objectId;
@property (nonatomic, strong) NSString* message;
@property (nonatomic, strong) NSString* labelOne;
@property (nonatomic, strong) NSString* labelTwo;
@property (nonatomic, strong) NSString* linkButton;
@property (nonatomic, strong) NSString* labelButton;

+ (NSArray *)downloadImageFromServer;

+ (LCDownloadImage *)hello:(NSString *)one and:(NSString *)two and:(NSString *)three;

+ (LCDownloadImage *)hi:(NSString *)labelOne and:(NSString *)labelTwo and:(NSString *)linkButton and:(NSString *)labelButton andimageUrl:(NSString *)imageurl;



@end

