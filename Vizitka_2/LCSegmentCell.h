//
//  LCSegmentCell.h
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SegmentDelegate;

@interface LCSegmentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISegmentedControl *timeSegment;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic) id<SegmentDelegate> delegate;

@end

@protocol SegmentDelegate <NSObject>

- (IBAction)actionSegment:(id)sender;


@end
