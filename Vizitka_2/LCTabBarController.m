//
//  LCTabBarController.m
//  Vizitka_2
//
//  Created by Vio on 08.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCTabBarController.h"

@interface LCTabBarController () <UITabBarControllerDelegate>

@end

@implementation LCTabBarController 

- (void)viewDidLoad {
    [super viewDidLoad];

    self.delegate = self;
    
    [[UITabBar appearance] setTintColor:[UIColor applicationTabBarItemsColor]];
   
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    NSArray *tabViewControllers = tabBarController.viewControllers;
    UIView * fromView = tabBarController.selectedViewController.view;
    UIView * toView = viewController.view;
    if (fromView == toView)
        return false;
    NSUInteger fromIndex = [tabViewControllers indexOfObject:tabBarController.selectedViewController];
    NSUInteger toIndex = [tabViewControllers indexOfObject:viewController];
    
    [UIView transitionFromView:fromView
                        toView:toView
                      duration:0.3
                       options: toIndex > fromIndex ? UIViewAnimationOptionTransitionCrossDissolve : UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished) {
                        if (finished) {
                            tabBarController.selectedIndex = toIndex;
                        }
                    }];
    return true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
