//
//  LCABLabelCell.h
//  Vizitka_2
//
//  Created by Vio on 01.12.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCABLabelCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *abLabel;

@end
