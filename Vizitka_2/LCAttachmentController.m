//
//  LCAttachmentController.m
//  Vizitka_2
//
//  Created by Vio on 05.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCAttachmentController.h"
#import "DBAttachmentPickerController.h"
#import "DBAttachment.h"
#import "NSDateFormatter+DBLibrary.h"
#import "LCAlertService.h"

static NSString *const kAttachmentCellIdentifier = @"AttachmentCellID";

@interface AttachmentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;

@end

@implementation AttachmentCell

@end

@interface LCAttachmentController ()

@property (strong, nonatomic) DBAttachmentPickerController *pickerController;

@end

@implementation LCAttachmentController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem* leftItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                 style:UIBarButtonItemStylePlain target:self action:@selector(backToMain:)];
    
    [self.navigationItem setLeftBarButtonItem:leftItem];
    
    [[LCAlertService instance] showAlertButtonOkWithTitle:kWarning andMessage:kSizeFile inVC:self];
    
}

#pragma mark -

- (IBAction)addAttachmentButtonDidSelect:(UIBarButtonItem *)sender {
    
    UIView *senderView = [sender valueForKey:@"view"];
    __weak typeof(self) weakSelf = self;
    DBAttachmentPickerController *attachmentPickerController = [DBAttachmentPickerController attachmentPickerControllerFinishPickingBlock:^(NSArray<DBAttachment *> * _Nonnull attachmentArray)
    {
        
        NSMutableArray *indexPathArray = [NSMutableArray arrayWithCapacity:attachmentArray.count];
        NSUInteger currentIndex = weakSelf.attachmentArray.count;
        for (NSUInteger i = 0; i < attachmentArray.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:currentIndex+i inSection:0];
            [indexPathArray addObject:indexPath];
        }
        [weakSelf.attachmentArray addObjectsFromArray:attachmentArray];
        [weakSelf.tableView insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationAutomatic];

    } cancelBlock:nil];
    
    attachmentPickerController.mediaType = DBAttachmentMediaTypeImage | DBAttachmentMediaTypeVideo | DBAttachmentMediaTypeOther;
    attachmentPickerController.capturedVideoQulity = UIImagePickerControllerQualityTypeHigh;
    attachmentPickerController.senderView = senderView;
    attachmentPickerController.allowsMultipleSelection = YES;
    attachmentPickerController.allowsSelectionFromOtherApps = YES;
    
    [attachmentPickerController presentOnViewController:self];
    
}

- (void)backToMain:(id)sender {
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController popViewControllerAnimated:NO];
    
}
#pragma mark - UITableView DataSource && Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.attachmentArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AttachmentCell *cell = [tableView dequeueReusableCellWithIdentifier:kAttachmentCellIdentifier];
    if (cell == nil) {
        cell = [[AttachmentCell alloc] init];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(AttachmentCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    DBAttachment *attachment = self.attachmentArray[indexPath.row];
    
    cell.titleLabel.text = attachment.fileName;
    cell.sizeLabel.text = attachment.fileSizeStr;
    cell.dateLabel.text = [[NSDateFormatter localizedDateTimeFormatter] stringFromDate:attachment.creationDate];
    
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize scaledThumbnailSize = CGSizeMake( 80.f * scale, 80.f * scale );
    
    [attachment loadThumbnailImageWithTargetSize:scaledThumbnailSize completion:^(UIImage *resultImage) {
        cell.thumbnailImageView.image = resultImage;
    }];
    
  
    if (attachment.mediaType == DBAttachmentMediaTypeImage){

       [attachment loadOriginalImageWithCompletion:^(UIImage * _Nullable resultImage) {
            [self.imagearray addObject:resultImage];
        }];
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.attachmentArray removeObjectAtIndex:indexPath.row];
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                     withRowAnimation:YES];
}

@end
