//
//  LCNumberAndEmailCell.m
//  Vizitka_2
//
//  Created by Vio on 14.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCNumberAndEmailCell.h"
#import "LCValidationService.h"

@interface LCNumberAndEmailCell ()

@property (assign, nonatomic) BOOL checkAt;
@property (strong, nonatomic) NSString* stringNew;
@property (assign, nonatomic) CGFloat widthWindow;
@property (assign, nonatomic) BOOL isAppear;

@end

@implementation LCNumberAndEmailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.isAppear = YES;
    self.checkAt = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.widthWindow = self.contentView.frame.size.width;
    
    if (self.isAppear) {
        [self prepareCell];
    }
}

- (void) prepareCell {
    
    CGFloat percentSize;
    if (self.widthWindow == 320) {
        percentSize = 1.f;
    } else if (self.widthWindow == 375) {
        percentSize = 1.17f;
    } else if (self.widthWindow == 414) {
        percentSize = 1.3f;
    }
    
    [self.phoneLabel setFont:[UIFont systemFontOfSize:11 * percentSize]];
    [self.mailLabel setFont:[UIFont systemFontOfSize:11 * percentSize]];
    
    self.isAppear = NO;
    
}


#pragma mark - UITextFieldDelegate


- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    self.checkAt = YES;
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField.tag == 0) {
        
        [self.phoneTextField becomeFirstResponder];
        
    }
    if (textField.tag == 1) {
        
        [self.mailTextField resignFirstResponder];
        
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    if (totalString.length == 15) {
        
        if ([string isEqualToString:@""]) {
            
        } else {
            
            [self.mailTextField becomeFirstResponder];
        }
    }
    
    if(textField == self.phoneTextField) {
        
        if (range.length == 1) {
            textField.text = [LCValidationService validateNumberPhoneWithNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [LCValidationService validateNumberPhoneWithNumber:totalString deleteLastChar:NO];
        }
        return false;
    }
    
    if ([textField isEqual:self.mailTextField]) {
        
        NSMutableString* nonValidateSymbol = [[NSMutableString alloc] initWithString:@"^|!#$%&*(){}[]?/<>=№`+ ;:'!~"];
        
        NSMutableCharacterSet *validationSet = [NSMutableCharacterSet characterSetWithCharactersInString:nonValidateSymbol];
        
        NSArray* validComponents = [string componentsSeparatedByCharactersInSet:validationSet];
        
        if ([validComponents count] > 1) {
            return NO;
        }
        
        if (textField.text.length == 0 && [string isEqualToString:@"@"] && [string isEqualToString:@"."]) {
            return NO;
            
        } else if ([string isEqualToString:@"%"] && self.checkAt) {
            self.checkAt = NO;
            
        } else if ([string isEqualToString:@"@"] && !self.checkAt) {
            return NO;
        }
        
        self.stringNew = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSCharacterSet* setAt = [NSCharacterSet characterSetWithCharactersInString:@"@"];
        NSArray* checkAt = [self.stringNew componentsSeparatedByCharactersInSet:setAt];
        
        
        if ([checkAt count] > 2) { 
            return NO;
        }
        
        return self.stringNew.length <= 56;
    }
    
    return NO;
    
}

- (IBAction)actionSendData:(id)sender {
        
    if ([self.delegate respondsToSelector:@selector(actionSendData:)]) {
        
        [self.delegate actionSendData:sender];
    }
    
}


@end
