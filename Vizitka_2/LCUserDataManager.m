//
//  LCUserDataManager.m
//  Vizitka_2
//
//  Created by Vio on 20.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCUserDataManager.h"

@implementation LCUserDataManager

+ (LCUserDataManager *)sendToServerUserInfoContactPhone:(NSString *)contactPhone andContactName:(NSString *)contactName andTime:(NSString *)time andAttachment:(NSString *)attachment; {
    
    LCUserDataManager* user = [LCUserDataManager new];

    user.contactPhone = contactPhone;
    user.contactName = contactName;
    user.suitebleTime = time;
    user.attachmentText = attachment;
    
    return user;
}

@end
