//
//  LCAAPhotoCell.h
//  Vizitka_2
//
//  Created by Vio on 01.12.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DownloadButtonDelegate;

@interface LCAAPhotoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *aaPhoto;
@property (weak, nonatomic) IBOutlet UILabel *labelFirstWindow;

@property (nonatomic) id<DownloadButtonDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *downloadButton;

@end

@protocol DownloadButtonDelegate <NSObject>

@optional

- (IBAction)downloadAction:(id)sender;

@end
