//
//  LCPortfolioDataDownload.h
//  Vizitka_2
//
//  Created by Vio on 30.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LCPortfolioDataDownload : NSObject

@property (strong, nonatomic) NSString* nameProject;

@property (strong, nonatomic) NSString* imageURLOne;
@property (strong, nonatomic) NSString* imageURLTwo;
@property (strong, nonatomic) NSString* imageURLThree;

@property (nonatomic, strong) NSString* labelOne;
@property (nonatomic, strong) NSString* labelTwo;

@property (nonatomic, strong) NSString* serialNumber;

@property (assign, nonatomic) NSInteger count;

@property (assign, nonatomic) NSInteger red;
@property (assign, nonatomic) NSInteger green;
@property (assign, nonatomic) NSInteger blue;

@property (strong, nonatomic) NSString* urlDownloadApp;


+ (NSArray *)downloadContentFromServer;

+ (LCPortfolioDataDownload *)hello:(NSInteger)one hello:(NSInteger)two hello:(NSInteger)three;

+ (LCPortfolioDataDownload *)hello:(NSString *)one;

@end
