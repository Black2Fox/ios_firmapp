//
//  LCPriceOrder.h
//  Vizitka_2
//
//  Created by Vio on 28.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LCPriceOrder : NSObject

@property (weak, nonatomic) NSString* position;
@property (weak, nonatomic) NSString* resultPrice;
@property (weak, nonatomic) NSString* userPhone;
@property (weak, nonatomic) NSString* userEmail;


+ (LCPriceOrder *)sendToServerInfoOrder:(NSString *)uPosition andResultPrice:(NSString *)uPrice phone:(NSString *)uPhone email:(NSString *)uEmail;
@end
