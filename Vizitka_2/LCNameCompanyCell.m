//
//  LCNameCompanyCell.m
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCNameCompanyCell.h"
#import "LCValidationService.h"
#import "LCAlertService.h"

@interface LCNameCompanyCell ()

@property (assign, nonatomic) BOOL isAppear;
@property (assign, nonatomic) BOOL checkAt;
@property (assign, nonatomic) CGFloat widthWindow;
@property (strong, nonatomic) NSString* stringNew;

@end

@implementation LCNameCompanyCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.isAppear = YES;
    self.checkAt = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.widthWindow = self.contentView.frame.size.width;
    
    if (self.isAppear) {
        [self prepareCell];
    }
}

- (void) prepareCell {
    
    CGFloat percentSize;
    if (self.widthWindow == 320) {
        percentSize = 1.f;
    } else if (self.widthWindow == 375) {
        percentSize = 1.17f;
    } else if (self.widthWindow == 414) {
        percentSize = 1.3f;
    }
    
    [self.phoneLabel setFont:[UIFont systemFontOfSize:11 * percentSize]];
    [self.skypeLabel setFont:[UIFont systemFontOfSize:11 * percentSize]];
    [self.emailLabel setFont:[UIFont systemFontOfSize:11 * percentSize]];
    
    self.isAppear = NO;
    
}


- (IBAction)actionPhone:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(actionPhone:)]) {
        
        [self.delegate actionPhone:sender];
    }
    
    
}
- (IBAction)callAction:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(callAction:)]) {
        
        [self.delegate callAction:sender];
    }
}

- (IBAction)emailAction:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(emailAction:)]) {
        
        [self.delegate emailAction:sender];
    }
    
}

- (IBAction)skypeAction:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(skypeAction:)]) {
        
        [self.delegate skypeAction:sender];
    }
    
}
@end
