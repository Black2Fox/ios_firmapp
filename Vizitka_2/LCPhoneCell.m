//
//  LCPhoneCell.m
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCPhoneCell.h"
#import "LCValidationService.h"

@interface LCPhoneCell ()

@property (assign, nonatomic) BOOL isAppear;
@property (assign, nonatomic) CGFloat widthWindow;
@property (assign, nonatomic) BOOL validationOk;

@end

@implementation LCPhoneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.isAppear = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.widthWindow = self.contentView.frame.size.width;
    
    if (self.isAppear) {
        [self prepareCell];
    }
}

- (void) prepareCell {
    
    CGFloat percentSize;
    if (self.widthWindow == 320) {
        percentSize = 1.f;
    } else if (self.widthWindow == 375) {
        percentSize = 1.17f;
    } else if (self.widthWindow == 414) {
        percentSize = 1.3f;
    }
    
    [self.labelPhone setFont:[UIFont systemFontOfSize:11 *percentSize]];
    [self.nameLabel setFont:[UIFont systemFontOfSize:11 *percentSize]];
    
    self.isAppear = NO;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
        
    if (self.nameLabel) {
        [self.phoneField becomeFirstResponder];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    if (totalString.length == 15) {
        
        if ([string isEqualToString:@""]) {
            
        } else {
            
            [self.phoneField resignFirstResponder];
        }
    }
    
    
    if(textField == self.phoneField) {
        if (range.length == 1) {
            textField.text = [LCValidationService validateNumberPhoneWithNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [LCValidationService validateNumberPhoneWithNumber:totalString deleteLastChar:NO];
        }
        return false;
    }
    
    return YES; 
}

@end
