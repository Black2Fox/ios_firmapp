//
//  LCUploadImage.m
//  Vizitka_2
//
//  Created by Vio on 15.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCDownloadImage.h"
#import "Backendless.h"

@implementation LCDownloadImage

+ (NSArray *)downloadImageFromServer {
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[LCDownloadImage class]];
    BackendlessCollection *collection = [dataStore find:nil];
    
    if (collection.data == nil) {
        NSArray* array = [NSArray new];
        return array;
    } else {
       return collection.data;
    }
    
}


+ (LCDownloadImage *)hello:(NSString *)one and:(NSString *)two and:(NSString *)three {
    LCDownloadImage *image = [LCDownloadImage new];
    image.labelOne = one;
    image.labelTwo = two;
    image.linkButton = three;
    return image;
    
}
+ (LCDownloadImage *)hi:(NSString *)labelOne and:(NSString *)labelTwo and:(NSString *)linkButton and:(NSString *)labelButton andimageUrl:(NSString *)imageurl{
    LCDownloadImage *image2 = [LCDownloadImage new];
    image2.labelOne = labelOne;
    image2.labelTwo = labelTwo;
    image2.linkButton = linkButton;
    image2.labelButton = labelButton;
    image2.imageURL = imageurl;
    return image2;
}
@end
