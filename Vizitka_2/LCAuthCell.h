//
//  LCAuthCell.h
//  Vizitka_2
//
//  Created by Vio on 23.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCAuthCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *buttonCabinet;
@property (weak, nonatomic) IBOutlet UIButton *buttonSocial;

@property (weak, nonatomic) IBOutlet UILabel *cabinetLabel;
@property (weak, nonatomic) IBOutlet UILabel *socialLabel;

@end
