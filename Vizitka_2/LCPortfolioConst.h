//
//  LCPortfolioConst.h
//  Vizitka_2
//
//  Created by Vio on 01.12.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LCAAPhotoCell.h"
#import "LCABLabelCell.h"
#import "LCABPhotoCell.h"
#import "LCNoConnectionStatusCell.h"
//ячейки
static NSString* const kIdentifireAAPhotoCell = @"aaPhotoCell";
static NSString* const kIdentifireABPhotoCell = @"abPhotoCell";
static NSString* const kIdentifireABLabelCell = @"abLabelCell";
static NSString* const kIdentifireNoConnectionCell = @"statusCell";


@interface LCPortfolioConst : NSObject

@end
