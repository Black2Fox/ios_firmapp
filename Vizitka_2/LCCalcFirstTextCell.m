//
//  LCCalcFirstTextCell.m
//  Vizitka_2
//
//  Created by Vio on 22.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCCalcFirstTextCell.h"
#import "LCPricesManager.h"

@interface LCCalcFirstTextCell ()

@property (assign, nonatomic) CGFloat widthWindow;
@property (assign, nonatomic) BOOL isAppear;

@end

@implementation LCCalcFirstTextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.isAppear = YES;
    
    self.firstTextLabel.text = @"Загрузка....";
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.activityIndicator startAnimating];
        self.tempArray = [LCPricesManager downloadPriceFromServer];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            for (LCPricesManager* manager in self.tempArray) {
                self.firstTextLabel.text = manager.generalLabel;
            }
            [self.activityIndicator stopAnimating];
            
        });
    });
    
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.widthWindow = self.contentView.frame.size.width;
    
    if (self.isAppear) {
        [self prepareCell];
    }
}


- (void) prepareCell {
    
    CGFloat percentSize;
    if (self.widthWindow == 320) {
        percentSize = 1.f;
    } else if (self.widthWindow == 375) {
        percentSize = 1.17f;
    } else if (self.widthWindow == 414) {
        percentSize = 1.3f;
    }
    
    [self.firstTextLabel setFont:[UIFont systemFontOfSize:12 * percentSize]];

    self.isAppear = NO;
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
