//
//  LCNumberAndEmailCell.h
//  Vizitka_2
//
//  Created by Vio on 14.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CollectAllPriceDelegate;

@interface LCNumberAndEmailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mailLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *mailTextField;

@property (weak, nonatomic) IBOutlet UIButton *buttonSend;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@property (nonatomic) id<CollectAllPriceDelegate> delegate;

@end
@protocol CollectAllPriceDelegate <NSObject>

@optional

- (IBAction)actionSendData:(id)sender;

@end
