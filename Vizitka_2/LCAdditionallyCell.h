//
//  LCAdditionallyCell.h
//  Vizitka_2
//
//  Created by Vio on 23.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCAdditionallyCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *buttonCloud;
@property (weak, nonatomic) IBOutlet UIButton *buttonInApp;
@property (weak, nonatomic) IBOutlet UIButton *buttonTeamWork;
@property (weak, nonatomic) IBOutlet UIButton *buttonPush;
@property (weak, nonatomic) IBOutlet UILabel *cloudLabel;
@property (weak, nonatomic) IBOutlet UILabel *inAppLabel;
@property (weak, nonatomic) IBOutlet UILabel *teamworkLabel;
@property (weak, nonatomic) IBOutlet UILabel *pushLabel;

@end
