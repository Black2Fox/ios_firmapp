//
//  LCPlatformDeviceCell.h
//  Vizitka_2
//
//  Created by Vio on 23.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCPlatformDeviceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *buttonPhone;
@property (weak, nonatomic) IBOutlet UIButton *buttonTablet;
@property (weak, nonatomic) IBOutlet UIButton *buttonWatch;

@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *tabletLabel;
@property (weak, nonatomic) IBOutlet UILabel *watchLabel;

@end
