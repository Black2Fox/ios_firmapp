//
//  LCLoadWindowController.m
//  Vizitka_2
//
//  Created by Vio on 07.12.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCLoadWindowController.h"
#import "JHUD.h"


#define JHUDRGBHexAlpha(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:(a)]

#define JHUDRGBA(r,g,b,a)     [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

@interface LCLoadWindowController ()
@property (weak, nonatomic) IBOutlet UIView *localAnimate;

@property (nonatomic) JHUD *hudView;

@property (nonatomic) CGRect orignalRect;

@end

@implementation LCLoadWindowController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.hudView = [[JHUD alloc]initWithFrame:self.localAnimate.bounds];
    
    [self circleJoinAnimation];
    self.localAnimate.backgroundColor = [UIColor clearColor];
    [self.view setBackgroundColor:[UIColor applicationSegmentsNopeHighlights]];
    [self performSelector:@selector(didChangeWindow) withObject:nil afterDelay:3.f];
    
}

- (void)circleJoinAnimation
{
    self.hudView.messageLabel.text = @"";
    self.hudView.messageLabel.backgroundColor = [UIColor clearColor];
    self.hudView.indicatorForegroundColor = JHUDRGBA(60, 139, 246, .5);
    self.hudView.indicatorBackGroundColor = [UIColor clearColor];//JHUDRGBA(185, 186, 200, 0.3);
    [self.hudView showAtView:self.localAnimate hudType:JHUDLoadingTypeCircleJoin];
    
}

- (void)customAnimation
{
    NSMutableArray * images = [NSMutableArray array];
    for (int index = 0; index<=19; index++) {
        NSString * imageName = [NSString stringWithFormat:@"%d.png",index];
        UIImage *image = [UIImage imageNamed:imageName];
        [images addObject:image];
    }
    
    self.hudView.indicatorViewSize = CGSizeMake(80, 80);
    self.hudView.customAnimationImages = images;
    [self.hudView showAtView:self.localAnimate hudType:JHUDLoadingTypeCustomAnimations];

}


- (void)didChangeWindow {

    [self performSegueWithIdentifier:@"showStartWindow" sender:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
