//
//  LCWebView.m
//  Vizitka_2
//
//  Created by Vio on 17.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCWebView.h"

@interface LCWebView () <UIWebViewDelegate>

@end

@implementation LCWebView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSURLRequest* request = [NSURLRequest requestWithURL:self.urlData];
    [self.webView loadRequest:request];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.indicator startAnimating];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.indicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.indicator stopAnimating];
    if (error) {
        
        [self createAlertTitle:@"Error" andMessage:[NSString stringWithFormat:@"Could not connect to the server\t Reload page"] andActionTitle:@"Sure"];
        
    }
    
}

- (void)createAlertTitle:(NSString *)title andMessage:(NSString *)message andActionTitle:(NSString *)actionTitle {
    
    UIAlertController* alert =
    [UIAlertController alertControllerWithTitle:title
                                        message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* action = [UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.webView reload];
                                                   }];
    
    [alert addAction:action];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
