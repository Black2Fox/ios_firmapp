//
//  LCCalculatorPush.m
//  Vizitka_2
//
//  Created by Vio on 14.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCCalculatorPush.h"
#import "LCHelpPush.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "LCAlertService.h"
#import "LCValidationService.h"

#import "LCPriceOrder.h"
#import "Backendless.h"

@interface LCCalculatorPush () <SendMailDelegate, MFMailComposeViewControllerDelegate, CollectAllPriceDelegate, UITableViewDelegate>

@property (assign, nonatomic) NSInteger fivePercentPrice;

@property (weak, nonatomic) UITextField* fieldPhone;
@property (weak, nonatomic) UITextField* fieldEmail;
@property (weak, nonatomic) NSIndexPath* customIndexPath;

@end

@implementation LCCalculatorPush

- (void)loadView {
    [super loadView];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createGesture];
    [self prepareController];
    
}


- (void)prepareController {
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LCMainPriceCell" bundle:nil] forCellReuseIdentifier:kIdentifireMainPriceCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCCallbackCell" bundle:nil] forCellReuseIdentifier:kIdentifireCallbackCell];
    [self.tableView registerNib:[UINib nibWithNibName:@"LCNumberAndEmailCell" bundle:nil] forCellReuseIdentifier:kIdentifireNumberEmailCell];
    
    self.navigationController.navigationBar.tintColor = [UIColor applicationBarColor];
    self.tableView.backgroundColor = [UIColor applicationMainBackgroundColor];
    
    self.navigationItem.title = @"Калькулятор";
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 0) {
        
        LCMainPriceCell* cell = (LCMainPriceCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireMainPriceCell];
        
        if (!cell) {
            cell = [[LCMainPriceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireMainPriceCell];
        }
        
        cell.contentView.backgroundColor = [UIColor applicationMainBackgroundColor];
        cell.firstPriceLabel.text = [self.controlPrice stringByAppendingString:@" руб."];
        NSInteger price = [cell.firstPriceLabel.text integerValue];
        self.fivePercentPrice = price * 0.95f;
        cell.secondPriceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.fivePercentPrice];

        return cell;
        
    }
    
    if (indexPath.section == 1) {
        
        LCCallbackCell* cell = (LCCallbackCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireCallbackCell];
        if (!cell) {
            cell = [[LCCallbackCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireCallbackCell];
        }
        
        [cell.contentView setTintColor:[UIColor applicationMainBackgroundColor]];
        
        cell.contentView.backgroundColor = [UIColor applicationMainBackgroundColor];
        
        cell.delegate = self;
        
        return cell;
    }
    
    
    if (indexPath.section == 2) {
        
        LCNumberAndEmailCell* cell = (LCNumberAndEmailCell *)[tableView dequeueReusableCellWithIdentifier:kIdentifireNumberEmailCell];
        if (!cell) {
            cell = [[LCNumberAndEmailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifireNumberEmailCell];
        }
        
        cell.contentView.backgroundColor = [UIColor applicationMainBackgroundColor];
        cell.delegate = self;
        
        self.fieldPhone = cell.phoneTextField;
        self.fieldEmail = cell.mailTextField;
        
        return cell;
        
    }
    else {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"123"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"123"];
        }
        
        return cell;
    }
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case 0:
            return 170.f;
            
        case 1:
            return 155.f;
            
        case 2:
            return 180.f;
    }
    
    return 44.f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0){
        
        [LCHelpPush settingPriceCell:(LCMainPriceCell *)cell];
    }
    
    if (indexPath.section == 1) {
        
        [LCHelpPush settingCallbackCell:(LCCallbackCell *)cell];
    }
    
    if (indexPath.section == 2) {
        
       [LCHelpPush settingNumberAndEmailCell:(LCNumberAndEmailCell *)cell];
    }
    
}

- (IBAction)actionEmail:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"Тема письма"];
        [mail setMessageBody:@"Текст сообщения" isHTML:NO];
        [mail setToRecipients:@[@"helpupd@gmail.com"]];
        
        [self presentViewController:mail animated:YES completion:NULL];
        
    } else {
        NSLog(@"error");
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)createGesture {
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(hideKeyBoard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
}

-(void)hideKeyBoard {
    [self.view endEditing:YES];
    
}

- (IBAction)actionSendData:(id)sender {

    [self.view endEditing:YES];
    
    NSString* tempUserPhone = [@"+7 " stringByAppendingString:self.fieldPhone.text];
    
    BOOL checkEmail;
    checkEmail = YES;
    
    if (!(self.fieldEmail.text.length == 0)) {
        checkEmail = [LCValidationService validationEmailWithString:self.fieldEmail.text];
    }
    
    BOOL checkPhone;
    
    if (tempUserPhone.length == 18) {
        checkPhone = YES;
    } else {
        checkPhone = NO;
    }
    
    if (checkEmail == YES && checkPhone == YES) {
        
        NSString* price = [NSString stringWithFormat:@"%@ / %ld", self.controlPrice, (long)self.fivePercentPrice];
      
        [backendless.persistenceService save:[LCPriceOrder sendToServerInfoOrder:self.position andResultPrice:price phone:self.fieldPhone.text email:self.fieldEmail.text]];
       
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
        
        [self clearAllFields];
        
        [[LCAlertService instance] showAlertButtonOkWithTitle:kSuccessSendTicket andMessage:kWaitCallCompany inVC:self];
        
    }
    if (checkEmail == NO) {
        [[LCAlertService instance] showAlertButtonOkWithTitle:kError andMessage:kFalseFilledEmail inVC:self];
    }
    if (checkPhone == NO) {
        [[LCAlertService instance] showAlertButtonOkWithTitle:kError andMessage:kFalseFilledPhone inVC:self];
    }

}

- (void)clearAllFields {
    
    self.fieldPhone.text = @"";
    self.fieldEmail.text = @"";
    
}

@end
