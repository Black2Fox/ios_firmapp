//
//  UIButton+LCCustomProperty.m
//  Vizitka_2
//
//  Created by Vio on 17.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//
static NSString * const kPropertyKey = @"NSURLCUSTOM";
static NSString * const kPropertyKeyTwo = @"PRICEVOLUME";

#import "UIButton+LCCustomProperty.h"
#import <objc/runtime.h>

@implementation UIButton (LCCustomProperty)

@dynamic customUrlValue, priceVolume;

- (void)setCustomUrlValue:(NSURL *)customUrlValue {
    objc_setAssociatedObject(self, (__bridge const void *)(kPropertyKey), customUrlValue, OBJC_ASSOCIATION_COPY);
    
}

- (NSString*)customUrlValue {
    return objc_getAssociatedObject(self, (__bridge const void *)(kPropertyKey));
}

- (void)setPriceVolume:(NSString *)priceVolume {
    objc_setAssociatedObject(self, (__bridge const void *)(kPropertyKeyTwo), priceVolume, OBJC_ASSOCIATION_COPY);
    
}

- (NSString*)priceVolume {
    return objc_getAssociatedObject(self, (__bridge const void *)(kPropertyKeyTwo));
}
@end
