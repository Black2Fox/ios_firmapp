//
//  LCConnectionStatus.m
//  Vizitka_2
//
//  Created by Vio on 16.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import "LCConnectionStatus.h"
#import <SystemConfiguration/SCNetworkReachability.h>

@implementation LCConnectionStatus

+ (BOOL)statusConnected {

    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address;
    address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com" );
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    
    bool canReach = success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    return canReach;

}

+ (UIAlertController *)noInternetConnection{
    
    UIAlertController* theAlertt = [UIAlertController alertControllerWithTitle:@"Внимание!"
                                                                       message:@"Проблема подключения. \n Часть контента будет недоступна."
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   
                               }];
    
    
    [theAlertt addAction:okAction];
   
    
    return theAlertt;
}
@end
