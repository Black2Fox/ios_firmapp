//
//  LCNameCompanyCell.h
//  Vizitka_2
//
//  Created by Vio on 07.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NameCompanyDelegate;

@interface LCNameCompanyCell : UITableViewCell <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameCompanyLabel;
@property (weak, nonatomic) IBOutlet UIView *mainViewCompany;

@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *skypeLabel;

@property (weak, nonatomic) IBOutlet UILabel *underPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *underEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *underSkypeLabel;

@property (weak, nonatomic) IBOutlet UIView *separatorOne;
@property (weak, nonatomic) IBOutlet UIView *separatorTwo;
@property (weak, nonatomic) IBOutlet UIView *separatorThree;

@property (weak, nonatomic) IBOutlet UILabel *otherLabel;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;

@property (nonatomic) id<NameCompanyDelegate> delegate;

@end

@protocol NameCompanyDelegate <NSObject>

@optional


- (IBAction)actionPhone:(id)sender;
- (IBAction)emailAction:(id)sender;
- (IBAction)skypeAction:(id)sender;
- (IBAction)callAction:(id)sender;

@end

