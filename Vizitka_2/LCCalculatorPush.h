//
//  LCCalculatorPush.h
//  Vizitka_2
//
//  Created by Vio on 14.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCCalculatorPush : UITableViewController

@property (strong, nonatomic) NSString* controlPrice;
@property (strong, nonatomic) NSString* position;

@end
