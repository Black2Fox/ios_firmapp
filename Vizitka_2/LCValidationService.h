//
//  LCValidationService.h
//  Vizitka_2
//
//  Created by Vio on 15.11.16.
//  Copyright © 2016 LightColor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LCValidationService : NSObject

@property (assign, nonatomic) BOOL validationOk;

+ (BOOL)validationEmailWithString:(NSString *)checkString;

+ (NSString *)validateNumberPhoneWithNumber:(NSString *) simpleNumber deleteLastChar:(BOOL)deleteLastChar;

@end
